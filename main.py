#!/usr/bin/env python3

import json
import logging

import sys
import app
import app.api
import app.logs
import suggestion_service
from suggestion_service import rulemining, suggestion


# will be changed and prepared at startup
LOGGER: logging.Logger = logging.getLogger()


def main():
	# example_setup()
	app_setup()


def app_setup():
	""" Work in progress implementation. """
	parser = app.ArgparseParser().get_parser()

	# show help message if no arguments were given
	if len(sys.argv) < 2:
		parser.print_usage()
		sys.exit(0)

	args = parser.parse_args()

	# prepare basic logger (suse = suggestion service)
	if args.logfile is not None:
		print(f'Writing logs to file: {args.logfile}')

	global LOGGER
	LOGGER = app.logs.prepare_logger(
		name='app',
		filepath=args.logfile,
		debug=args.debug
	)

	if args.command == 'mine':
		LOGGER.info('Loading Mining Service')
		rulemining_from_config_file(args.config_file, args.print)
	elif args.command == 'suggest':
		LOGGER.info('Loading Suggesting Service')
		suggestion_from_config_file(args.config_file, args.print, args.out)
	elif args.command == 'api':
		LOGGER.info('Starting API for Suggestion Service')
		api_from_config_file(
			args.config_file, args.hostname, args.port,
			add_cvr=args.cvr_endpoint, log_quiet=args.log_quiet
		)


def rulemining_from_config_file(config_file, printout):
	""" Start rule mining instance based on a config file. """

	# TODO: check can probably be removed soon when using schema validation
	app.Utils.check_json_file(config_file)
	conf = rulemining.config.RuleMiningConfig(config_file)
	arms = rulemining.RuleMiningService(verbosity=conf.verbosity)
	arms.generate_rules(conf.formrepo, conf.method, conf.rulerepo)
	conf.formrepo.close()
	conf.rulerepo.close()

	if printout:
		print('\nRules:')
		for r in conf.rulerepo.get_rules():
			print(r)


def suggestion_from_config_file(config_file, printout, output_file=None):
	""" Start suggestion service based on a config file. """

	out_path = None
	if output_file is not None:
		out_path = suggestion_service.io.IOUtils.prepare_file_write(output_file)
		if out_path is None:
			raise ValueError('Invalid output file!')

	# TODO: check can probably be removed soon when using schema validation
	app.Utils.check_json_file(config_file)
	conf = suggestion.config.SuggestionServiceConfig(config_file)
	pipeline = conf.pipeline
	pipeline.log_config()  # write current configuration to logs
	serv = suggestion.SuggestionService(pipeline)
	s = serv.generate_suggestions(conf.rulerepo, conf.populated, conf.target_field)
	conf.rulerepo.close()

	# print results
	if printout:
		print(f'\nSuggestions:\n{json.dumps(s)}')
		if conf.gather_stats:
			print(f'\nStats:\n{json.dumps(pipeline.get_stats())}')

	# write results to file
	if output_file is not None and out_path is not None:
		result = {'suggestions': s}
		if conf.gather_stats:
			result['stats'] = pipeline.get_stats()
		print(f'Writing results to file: {str(out_path)}')
		with out_path.open('w') as fout:
			json.dump(result, fout, indent=2)


def api_from_config_file(config_file, host=None, port=None, add_cvr=False, log_quiet=False):
	""" Start suggestion service with API based on config file. """

	# TODO: check can probably be removed soon when using schema validation
	app.Utils.check_json_file(config_file)
	conf = suggestion.config.SuggestionServiceConfig(config_file)
	conf.pipeline.log_config()  # write current configuration to logs
	serv = suggestion.SuggestionService(conf.pipeline)

	def generate_suggestions(populated, target: str):
		arsr_suggestions = serv.generate_suggestions(conf.rulerepo, populated, target)
		result = {'suggestions': arsr_suggestions}
		if conf.gather_stats: result['stats'] = conf.pipeline.get_stats()
		return result

	api = app.api.SuggestionAPI.setup(generate_suggestions)

	# add the Cedar VR scoring endpoint for evaluation
	if add_cvr:
		pipeline_config = conf.pipeline_config
		cvr_pipeline = suggestion.pipeline.CedarPipeline(
			discard_zero_score=pipeline_config.skip_zero_score(),
			gather_stats=pipeline_config.gather_stats()
		)
		serv_cvr = suggestion.SuggestionService(cvr_pipeline)

		def generate_suggestions_cvr(populated, target: str):
			cvr_suggestions = serv_cvr.generate_suggestions(conf.rulerepo, populated, target)
			result = {'suggestions': cvr_suggestions}
			if conf.gather_stats: result['stats'] = cvr_pipeline.get_stats()
			return result

		api.add_cedar_endpoint(generate_suggestions_cvr)
		LOGGER.info('Added Cedar VR endpoint.')

	log_requests = True
	if log_quiet:
		LOGGER.info('Setting log level to warning.')
		logging.getLogger().setLevel(logging.WARNING)
		log_requests = False

	# start the API
	api.run(host=host, port=port, log_requests=log_requests)
	conf.rulerepo.close()


def example_setup():
	""" Example setup using in-memory repositories. """

	# prepare association rule mining service, data and miner
	arms = rulemining.RuleMiningService()
	formrepo = rulemining.forms.repositories.InMemoryFormRepo.example_animals()
	rulerepo = rulemining.rules.repositories.InMemoryRuleRepo()
	apriori = rulemining.methods.MethodFactory.create(
		'apriori',
		minsupp=0.2,
		minconf=0.4
	)
	arms.generate_rules(formrepo, apriori, rulerepo)  # perform ARM

	# prepare suggestion service and data
	sugs = suggestion.SuggestionService.example_setup()
	populated = {'fly': 'yes'}
	target = 'tags'
	suggestions = sugs.generate_suggestions(rulerepo, populated, target)
	print(f'\nSuggestions:\n{json.dumps(suggestions)}')


if __name__ == '__main__':
	main()
