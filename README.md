# Association Rule Mining and Semantic Relatedness

Python implementation of the core ARSR modules for Rule Mining and Suggestion.


**Table of Contents**  
- [Setup](#setup)
  - [VirtualEnv](#virtualenv)
  - [Dependencies](#dependencies)
- [Run Standalone](#run-standalone)
  - [Run Examples](#run-examples)
- [Use as Library](#use-as-library)
- [Test](#test)
  - [Unit Testing](#unit-testing)
  - [Integration Testing](#integration-testing)
  - [Test Everything](#test-everything)
  - [Test Coverage](#test-coverage)
- [API](#api)
  - [Start API Server](#start-api-server)
  - [API Usage](#api-usage)
- [Other](#other)
  - [Code Style](#code-style)
  - [Logging](#logging)
- [Notes on Blazegraph (RDF store)](#notes-on-blazegraph)
  - [Blazegraph Usage](#blazegraph-usage)
  - [Blazegraph SPARQL Endpoint](#blazegraph-sparql-endpoint)

More documentation is available in the [docs](docs) folder.  
For instance, an [implementation overview](docs/img/Implementation-Overview.pdf)
and [information about configuration](docs/config).  



## Setup

Running the python scripts of this repository requires **`Python 3.6+`**!  
Furthermore, dependencies listed in `requirements.txt` must be installed (see below).  


### VirtualEnv

We use [virtualenv](https://virtualenv.pypa.io/en/latest/) for creating a virtual environment to manage dependencies installed with [pip](https://pypi.org/project/pip/).  
One way to install virtualenv is [via pip](https://virtualenv.pypa.io/en/latest/installation.html#via-pip): `pip install virtualenv`.  

Now create a virtual environment as follows.  
Move to the location of this repository and open a terminal in this folder.  
Create the environment with:
```
virtualenv venv -p python3
```

The `-p python3` part may not be required if it is your system's default.  

When using Powershell on **Windows**, now [set execution policies](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy)
so that the environment can be started.
Note that **you have to do this for every new terminal** you start:
```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process
```

(**Windows**) Start the environment by executing:
```
venv/Scripts/activate
```

(**Linux**) Start the environment by executing:
```
source venv/bin/activate
```

If successful, you should now see `(venv)` as a prefix of lines in the terminal.  


### Dependencies

Make sure to start the virtual environment before executing the following commands.  

If the suggestion service is used as a library,
dependencies are installed when running the `setup.py` script.  
In this case, no further installation is required.

Otherwise, use pip to install dependencies as follows:
```
pip install -r requirements.txt
```



## Run Standalone

Following sections describe how to run the application in case you don't want to install the package.
Make sure that [dependencies](#dependencies) are installed before running.  
For information about required input data, [read here: docs/data-format.md](docs/data-format.md).  

### Run Examples

To run a rule mining service example `rulemining/example.py`, use the following command.  
It executes a simple program that shows how to use the rule mining module.
```
python -m suggestion_service.rulemining.example
```

More examples can be executed using the `main.py` script.  
They are prepared as a configuration that can be loaded.  
Note that some examples require specific files to exist or even that the RDF store is available.  
Config files of such examples usually state this under "description".

For instance, the following starts a **rule mining** example similar to the previous:
```
python main.py mine ./configs/example/rm-conf-1.json -p
```

As another example, this will start the **suggestion** process:
```
python main.py suggest ./configs/example/su-conf-1.json -p
```

Lastly, an example for using the **API**:
```
python main.py api ./configs/example/api-conf-1.json
```

After starting the API service, one could send this `POST`-request to `http://localhost:8050/suggest`
```json
{
  "target_field": "tags",
  "populated_fields": {
    "type": "mammal"
  }
}
```

... and obtain the following response:
```json
{
  "success": true,
  "suggestions": {
    "claws": 0.6,
    "whiskers": 0.525
  }
}
```


### Run Suggestion Service with LDS-Provider

To run suggestion with LDS-provider (which uses the LDS library via our Java SemRel-API),  
an RDF store needs to be installed and proper RDF data loaded to it.  
We used Blazegraph in our evaluation, as it does not require more than Java to be installed and is easy to set up.  
The RDF store can be any other software but must provide a generic SPARQL endpoint.  
Instructions to set up the Java SemRel-API are available in its corresponding repository documentation.

Next requirement is a proper configuration file (see [docs/config/](docs/config) for more).  
An example for such a file is available in [configs/example/](configs/example), named `su-conf-3.json`.  
It is important to set the correct endpoint for the relatedness provider in this file like so:
```json
"relatedness_provider": {
    "name": "lds",
    "config": {
        "endpoint_url": "http://localhost:8080/api/relatedness/ldsd"
    }
}
```

Once the config is created and everything up and running (i.e. SPARQL endpoint and SemREL API), start the suggestion like previously with:
```
python main.py suggest <path_to_config_file>
```

Or use the API by switching `suggest` with `api` in the previous command.


## Use as Library

To install the `suggestion_service` (without API) as a library, execute the `setup.py` script.  
Instead of running the script directly, it is recommended to use `pip` as follows:
```
pip install .
```

This command installs required dependencies and allows to import the `suggestion_service` package.


### Basic Usage as Library

Following examples use hard-coded input data (in-memory repositories).  
The required input data format for loading data from a file is described [here](docs/data-format.md).

**Rule Mining:**
```
from suggestion_service import rulemining

# prepare data repositories (here we load an example)
formrepo = rulemining.forms.repositories.InMemoryFormRepo.example_animals()
rulerepo = rulemining.rules.repositories.InMemoryRuleRepo()

# configure apriori algorithm
apriori = rulemining.methods.Apriori(minsupp=0.3, minconf=0.7)

# set up service, start mining and print out rules
service = rulemining.RuleMiningService()
service.generate_rules(formrepo, apriori, rulerepo)
for rule in rulerepo.get_rules(): print(rule)
```

Find this example also in `suggestion_service/rulemining/example.py`.  

**Recommending:**
```
from suggestion_service import rulemining
from suggestion_service import suggestion

# prepare relatedness provider and scoring pipeline
relatedness_provider = suggestion.rproviders.BinaryProvider()  # just string-equality
pipeline_config = suggestion.pipeline.PipelineConfig({
  'field_aggregation_method': 'median',
  'score_aggregation_method': 'max'
})
pipeline = suggestion.pipeline.Pipeline(relatedness_provider, pipeline_config)

# prepare data: rules, populated form and target field
rulerepo = rulemining.rules.repositories.InMemoryRuleRepo.example_animals()
populated_fields = {'fly': 'no', 'type': 'mammal'}
target_field = 'tags'

# create service and create suggestions dict
suse = suggestion.SuggestionService(pipeline)
suggestions = suse.generate_suggestions(rulerepo, populated_fields, target_field)
print(suggestions)
```


## Test

### Unit Testing

```
python -m unittest
```

For more details during execution of test, use:
```
python -m unittest --verbose
```

*Note:*  
If a test file is not found it can be because of its name.  
E.g. a file named `test_my-test.py` is not found because of the `-`.  
Instead, use `test_my_test.py` or `test_mytest.py`.  
For more on that, see [unittest test discovery](https://docs.python.org/3/library/unittest.html#unittest-test-discovery).  


### Integration Testing

Note that integration tests require a specific setup.  
Have a look at the comments in the corresponding test files for more information.  

Run integration tests using:
```
python -m unittest discover -s integration -p itest_*
```

Or
```
python -m unittest discover -p itest_*
```


### Test Everything

To run both kinds of tests (note that some need specific datasets in database), use:
```
python -m unittest discover -p *test_*py
```


### Test Coverage

For test coverage, use the [`coverage` package](https://github.com/nedbat/coveragepy).  
If not already installed through the requirements file, use the following to install: `pip install coverage`.  

After that, run the tests as follows:
```
coverage run -m unittest discover
```

Now generate the coverage report:
```
coverage report
```



## API


### Start API Server

HTTP-API for suggestions can be started with:
```
python main.py api configs/example/api-conf-1.json -port 8050
```

Note that the path to the config file should be set accordingly.  
If `-port` is not given, `8050` is used as default.  
The hostname can be changed as well using `-host` or `--hostname`.  


### API Usage

Send a `POST` request to `http://localhost:8050/suggest` with:
```
Content-Type: application/json
Body:
{
    "populated_fields": {
        "field 1": "value 1",
        "field 2": ["value 1", "value 2"],
        ...
    },
    "target_field": "name of target field"
}
```
Note that the body content must be valid JSON.

**On success**, the response will look as follows:
```json
{
    "success": true,
    "suggestions": {}
}
```

**On failure**, the response looks like this:
```json
{
    "success": false,
    "reason": "Reason of failure"
}
```

The content of `"suggestions"` depends on the configuration of the suggestion service.  
In the simple case, a complete response will look like this:  
```json
{
    "success": true,
    "suggestions": {
        "suggestion 1": 0.6,
        "suggestion 2": 0.525
    }
}
```



## Other

### Code Style

We don't stricly follow any code style like PEP8 but try our best to be consistent.  

To run a style check using `pycodestyle` (formerly pep8), first install it:
```
pip install pycodestyle
```

After that, run the following command using the provided config file:
```
pycodestyle --config pycodestyle.ini .
```

### Logging

We use the `logging` package to log messages.  
The library (`suggestion_service`) itself has some logging (e.g. in services or API) but only uses the default names `__name__`.  
This means one can simply access the package main logger and disable it like this:
```python
import logging
logger = logging.getLogger('suggestion_service')
logger.propagate = False # disable propagating to root logger
```


## Notes on Blazegraph

### Blazegraph Usage

Using blazegraph is simple.
Just download the [executable JAR file "blazegraph.jar"](https://github.com/blazegraph/database/releases)
and execute it using the command:
```
java -server -jar filename.jar
```

You can change some settings like the port using `-D`.  
For instance, to disable jindi (read about log4shell for why to do it):
```
java -server -"Dlog4j2.enableJndi"=false -jar filename.jar
```

Or to change the port:
```
java -server -D"jetty.port"=<port> -jar filename.jar
```

After starting the server, you can access the web interface at:
```
http://localhost:9999/blazegraph
```


### Blazegraph SPARQL Endpoint

For more detailed information, refer to the [REST API documentation](https://github.com/blazegraph/database/wiki/REST_API).  
Please make sure to replace `bigdata` in example commands with `blazegraph`.  

For instance, the SPARQL endpoint of blazegraph is:
```
http://localhost:9999/blazegraph/sparql
```

To use [namespaces](https://github.com/blazegraph/database/wiki/REST_API#multi-tenancy-api) (graph names), use the endpoint:
```
http://localhost:9999/blazegraph/namespace/YOUR_NAMESPACE/sparql
```

For queries, we can use `GET` or `POST` requests ([see here](https://github.com/blazegraph/database/wiki/REST_API#query)).  
Note that using `GET` requests, results will be cached.  
As an example using `POST` and retrieving result as `Json`:
```
curl -X POST http://localhost:9999/blazegraph/sparql --data-urlencode 'query=SELECT * WHERE { ?s ?p ?o } LIMIT 1' --data-urlencode 'format=json'
```

The `format` query parameter does not support many types ([see here](https://github.com/blazegraph/database/wiki/REST_API#get-or-post)).  
Instead, we can add `Accept` to the request header for using other mime types:
```
curl -X POST http://localhost:9999/blazegraph/sparql --data-urlencode 'query=SELECT * WHERE { ?s ?p ?o } LIMIT 1' --header "Accept: application/json"
```
This only works if `format` is **not** given!  
