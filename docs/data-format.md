# Input Data Format
This document describes the file format of input data (i.e. [form instances](#form-instances) and [rules](#rules)) as of May 2022.


## Form Instances
This section describes the format of form instances loaded by the FileFormRepo.    

Currently, only the JSON format is supported.  
Future work could also support JSON Lines (.jsonl) format.  
We already implemented this support in the FileRuleRepo
and this existing implementation should be used for reference.

This is the basic JSON structure of a form instance repository file:
```json
{
  "instances": [
    <FORM INSTANCE>,
    <FORM INSTANCE>,
    ...
  ]
}
```

Each form instance uses the following JSON format:
```json
{
  "fields": {
    "field1": {
      "type": "field type (optional)",
      "values": ["v1"]
    },
    "field2": {
      "values": ["v1", "v2"]
    }
  }
}
```

Fieldnames must be unique and the same for the same field across multiple form instances.


## Rules
This section describes the format of rules loaded by the FileRuleRepo.    

It currently supports JSON (.json) and JSON Lines (.jsonl) format.
We suggest using the JSON Lines format, as it is easier to process and compress.

This is the basic JSON structure:
```json
{
  "rules": [
    <RULE>, 
    <RULE>,
    ...
  ]
}
```

Rules in JSON Lines format are stored as follows in the file.  
Note that `newline` separates the rules. Each line is one rule in JSON format (see below).
```
<RULE>
<RULE>
...
```

Each `<RULE>` uses the following JSON format.  
The `lhs` (left-hand side) is the rule antecedent and `rhs` (right-hand side) the consequent.  
Both of them can have one or multiple entries but at least one for each must be given.  
Furthermore, `sup` is the rule support and `con` the confidence in range 0 to 1.
```json
{
  "lhs": [
    {
      "field": "field1 name",
      "value": ["v1", "v2"]
    }
  ],
  "rhs": [
    {
      "field": "field2 name",
      "value": ["v3"]
    }
  ],
  "sup": 0.6,
  "con": 1.0
}
```
