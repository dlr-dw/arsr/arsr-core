# Suggestion Configuration

E.g. used here: `python main.py suggest <config_file>`.  
The file is processed by the class `suggestion_service.suggestion.config.SuggestionServiceConfig`.  

Basic structure of `config`:
```json
"config": {
	"target_field": "",
	"populated": {},
	"rulerepo": {
		"name": "",
		"config": {}
	},
	"relatedness_provider": {
		"name": "",
		"config": {}
	},
	"pipeline": {}
}
```

*Parameters*:
- `target_field`: Name of target field to make suggestions for.
- [`populated`](#populated): Populated field information that suggestions are based on.
- [`rulerepo`](#rulerepo): Rule repository to load association rules from.
- [`relatedness_provider`](#relatedness_provider): Configures relatedness provider to use for relatedness scoring.  
- [`pipeline`](#pipeline): Configures the scoring pipeline.

Find details about each parameter below.  
An [example](#example) configuration is available at the end of this document.  



## populated
A JSON object to provide the populated fields.  

The currently supported structure looks as follows:
```json
"populated": {
	"field1": "value1",
	"field2": ["v2a", "v2b", ...],
	"field3": ["value3"]
}
```

Note that there is **no structure validation** yet.  
Please make sure to pass the correct format.  
Field types are currently **not** supported (future work).  



## rulerepo
Configures the **rule repository** used to store the generated association rules.   
See [Rule Repository Documentation](config_rulerepo.md) for details.  



## relatedness_provider
Configures the provider for relatedness computation.  
Relatedness is computed for two entities (or terms).  
A simple equality-based relatedness and more advanced semantic relatedness provider is available.  

```json
"relatedness_provider": {
	"name": "",
	"config": {}
}
```

Currently supported entries for `name` are: `binary`, `lds`.   
The input of `config` depends on the selected name (see below).  

For further implementation details see `suggestion_service.suggestion.rproviders.factory`.


### relatedness_provider: binary
Use simple string-equality based provider with relatedness being either 0 (no match) or 1 (match).  
No further configuration is required.


### relatedness_provider: lds
Semantic relatedness based provider using the `SemRel Java API`.  
This API provides approaches like `LDSD` and `Resource Similarity (Resim)` implemented in the [lds Java library](https://github.com/FouadKom/lds).

*Required Parameters:*  
- `endpoint_url`: str - Address of the API endpoint.

*Optional Parameters:*
- `require_uris`: bool (true) - If an entity value is no URI, string-similarity is used instead.
- `request_timeout`: float - Request timeout in seconds.
- `cache`: object - Cache configuration (see below).

#### lds: cache
Allows to configure a cache to use for relatedness retrieval.  
This can speed up the runtime especially if computation is expensive.  
The cache stores recent relatedness results of entity-pairs.  

Basic structure:
```json
"cache": {
	"type": "lru",
	"maxsize": 1000
}
```

*Parameters:*  
- `type`: str (lru) - Type of cache, one of: `lru`, `lfu`, `ttl`.
- `maxsize`: int (200) - Maximum size of the cache.
- `ttl`: float (300) - Time (seconds) cached items are kept before removal (only for type ttl).



### pipeline
Configure the scoring pipeline.  
In theory, multiple pipelines are possible and currently two exist.  
However, we only allow configuring our default pipeline implementation for now.  

The configuration of this pipeline is validated using a JSON Schema.  
Find the config source code here: `suggestion_service.suggestion.pipeline.pipline_config`.  
Find the JSON schema here: `suggestion_service.suggestion.pipeline.pipline_config_schema`.  

*Parameters*:
- `field_aggregation_method`: str - Name of method for the first (field score) aggregation.
- `score_aggregation_method`: str - Name of method for the second (value score) aggregation.
- `evaluation`: bool (false) - Set evaluation mode. Uses all aggregation methods, i.e. respective parameters are ignored.
- `skip_zero_score`: bool (true) - Discard rules with final score of zero.
- `skip_no_pairs`: bool (true) - Discard rules without entity pairs.
- `jaccard_threshold`: float (0.5) - Discard rules with premise jaccard index below this value.
- `discard_already_populated`: bool (true) - Discard suggestions already in populated field (if it equals target field).
- `final_score`: object - Configure how value recommendation score is computed from context matching score (cms).
  - `apply_confidence`: bool (true) - Apply (multiply with) rule confidence.
  - `apply_jaccard_index`: bool (true) - Apply (multiply with) jaccard index of rule premise.
  - `apply_confidence_fit`: bool (false) - Apply (multiply with) fit of rule consequent.



## Example

```json
{
	"name": "Suggestion Config",
	"description": "Example suggestion configuration",
	"config": {
		"target_field": "tags",
		"populated": {
			"type": "http://dbpedia.org/ontology/Mammal"
		},
		"rulerepo": {
			"name": "file",
			"config": {
				"filepath": "data/example/rules/animal-ex1-rules.json",
				"save_on_close": false
			}
		},
		"relatedness_provider": {
			"name": "lds",
			"config": {
				"endpoint_url": "http://localhost:8080/api/relatedness/ldsd",
				"cache": {
					"type": "lru",
					"maxsize": 512
				},
				"request_timeout": 60
			}
		},
		"pipeline": {
			"field_aggregation_method": "median",
			"score_aggregation_method": "max",
			"jaccard_threshold": 0.5
		}
	}
}
```
