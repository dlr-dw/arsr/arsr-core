# Configuration Files

This document explains how to create a valid configuration file.  
Such a file is loaded when starting the suggestion service using `main.py`.  
For examples, have a look at the folder `suggestion-service/configs/examples`.  


## Basic Structure

Each configuration file follows the following basic structure:
```json
{
	"name": "arbitrary config name",
	"description": "optional description",
	"config": {}
}
```

The information of field `config` is further specified based on the use case:
- [Mining Configuration](config_mining.md)
- [Suggestion Configuration](config_suggestion.md)
- [API Configuration](config_api.md)
