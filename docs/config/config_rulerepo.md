# Rule Repository

A rule repository stores the association rules.  
It is configured as part of the main configurations
(i.e. api, mining and suggestion configs).  
The corresponding JSON attribute is called `rulerepo`:
```json
{
	"rulerepo": {
		"name": "",
		"config": {}
	}
}
```
  
Supported entries for `name` are: `in-memory`, `file`.   
Input of `config` depends on the selected name (see below).  

For more see: `suggestion_service.rulemining.rules.repositories.factory`.  


## In-Memory Rule Repository
Pure in-memory repository to store rules temporarily.  

Use `"name": "in-memory"` for this rule repository.  
No further configuration is required.  

*Optional Parameters:*  
- `example`: str - currently, only `animals` is allowed as value to load animal example rules


## File Rule Repository
Loads the rules from a given file and stores them in memory.  

Use `"name": "in-memory"` for this rule repository.

Example config structure:
```json
"config": {
	"filepath": "path/file.json or path/file.jsonl"
}
```

Note that `JSON` (.json) and `JSON Lines` (.jsonl) files are supported.  
The format is detected based on the file extension, so make sure to use the correct one.  

*Optional Parameters:*  
- `load_now`: bool (true) - Load file on instance creation (should be `true` if not used from within code)
- `save_on_close`: bool (true) - Save (and overwrite) the contents on close, otherwise discard
