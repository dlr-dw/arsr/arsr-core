# Mining Configuration

E.g. used here: `python main.py mine <config_file>`.  
The file is processed by the class `suggestion_service.rulemining.config.RuleMiningConfig`.  

Basic structure of `config`:
```json
"config": {
	"formrepo": {
		"name": "",
		"config": {}
	},
	"method": {
		"name": "",
		"config": {}
	},
	"rulerepo": {
		"name": "",
		"config": {}
	}
}
```

*Parameters*:
- [`formrepo`](#formrepo): Configures the form repository used to retrieve input form instances.
- [`method`](#method): Configures the used association rule mining approach.
- [`rulerepo`](#rulerepo): Configures the rule repository used to store the generated rules.

An [example](#example) is available at the end of this document.  



## formrepo
Configures the **form repository** used to retrieve input form instances.  
Supported entries for `name`: `in-memory`, `file`.  
For more see: `suggestion_service.rulemining.forms.repositories.factory`.  
Input of `config` depends on the selected name.  


#### formrepo: in-memory
Use in-memory form repository.  
No further configuration is required.  

*Optional Parameters:*  
- `example`: str - currently, only `animals` is allowed as value to load animal example


#### formrepo: file
Use form repository from file.  

Example config structure:
```json
"config": {
	"filepath": "path/file.json"
}
```

Note that only `JSON` (.json) files are supported.  

*Optional Parameters:*  
- `load_now`: bool (true) - Load file on instance creation (should be `true` if not used from within code)
- `save_on_close`: bool (true) - Save (and overwrite) the contents on close, otherwise discard



## **method**
Configures the used **association rule mining** approach.  
Currently supported entries for `name`: `apriori`.  
For more see: `suggestion_service.rulemining.methods.factory`.  


#### method: apriori
Use `Apriori` algorithm for rule mining.  

Example config structure:
```json
"config": {
	"minsupp": 0.3,
	"minconf": 0.6
}
```

*Optional Parameters:*  
- `minsupp`: float (0.5) - Minimum support for frequent itemset mining (range 0-1).
- `minconf`: float (0.5) - Minimum confidence for rule generation (range 0-1).
- `max_length`: int (8) - Maximum length k of k-itemsets and rules. Should be max. length of all transactions.
- `verbosity`: int (0) - Printout verbosity (if available).



## rulerepo
Configures the **rule repository** used to store the generated association rules.   
See [Rule Repository Documentation](config_rulerepo.md) for details.  



## Example

```json
{
	"name": "Rule Mining Config",
	"description": "Example rule mining configuration",
	"config": {
		"formrepo": {
			"name": "in-memory",
			"config": {
				"example": "animals"
			}
		},
		"method": {
			"name": "apriori",
			"config": {
				"minsupp": 0.3,
				"minconf": 0.6
			}
		},
		"rulerepo": {
			"name": "in-memory",
			"config": {}
		},
		"verbosity": 1
	}
}
```
