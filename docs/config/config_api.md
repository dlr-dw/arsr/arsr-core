# API Configuration

E.g. used here: `python main.py api <config_file>`.  
The file is processed by the class `suggestion_service.suggestion.config.SuggestionServiceConfig`.  

Note that this configuration is basically the same as the [suggestion configuration](config_suggestion.md).  
In contrast to it, it does not require the fields `target_field` and `populated`.  

The configuration file just configures how suggestions are made.  
For actual changes to the API settings, use the available command line arguments.  
Run the following to show them: `python main.py api --help`.  
