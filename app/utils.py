import os

class Utils:
	""" Class that provides some helper functions as class methods. """

	def check_json_file(filepath):
		""" Checks if path leads to a JSON file. """
		if not os.path.isfile(filepath):
			raise ValueError('Invalid config file!')
		if not filepath.lower().endswith('.json'):
			raise ValueError('Invalid config file (invalid format)!')
