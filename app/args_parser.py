import argparse

from .args_range import ArgRange


class ArgparseParser:
	""" Argument parser using argparse. """

	def __init__(self):
		self.parser = self.__prepare_parser()

	def get_parser(self):
		""" Returns the argparse parser instance. """
		return self.parser

	@staticmethod
	def __prepare_parser():
		""" Prepare argument parsing. """
		parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
		parser.add_argument('-lf', '--logfile', help='Path and name of log file', default='logs.log')
		parser.add_argument('-debug', '--debug', help='Set logging level to debug', action='store_true')

		# RULE MINING parser
		subparsers = parser.add_subparsers(dest='command')
		parser_rm = subparsers.add_parser('mine', help='Run rule mining service')
		parser_rm.add_argument('config_file', help='Path to JSON config file')
		parser_rm.add_argument('-p', '--print', help='Print results', action='store_true')

		# SUGGESTION SERVICE parser
		parser_su = subparsers.add_parser('suggest', help='Run suggestion service')
		parser_su.add_argument('config_file', help='Path to JSON config file')
		parser_su.add_argument('-p', '--print', help='Print results', action='store_true')
		parser_su.add_argument('-o', '--out', help='Path to JSON file for writing output', required=False, default=None)

		# Parser for Suggestion Service usage per API
		parser_api = subparsers.add_parser('api', help='Run suggestion service with API')
		parser_api.add_argument('config_file', help='Path to JSON config file for suggestion service')
		parser_api.add_argument('-host', '--hostname', help='Set the hostname (localhost if not set)', default=None)
		parser_api.add_argument('-port', '--port', help='Server port to use (default: 8050)', default=8050)
		parser_api.add_argument('-cvr', '--cvr-endpoint', help='Add Cedar VR scoring endpoint', action='store_true')
		parser_api.add_argument('-q', '--log-quiet', help='Log warnings and errors only', action='store_true')
		return parser
