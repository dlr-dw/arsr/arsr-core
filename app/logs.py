import sys
import logging


def prepare_logger(name, filepath=None, debug=False, file_debug=False):
	""" Prepare logging.

	Parameters
	----------
	name : str
		Name of the application logger.
	filepath : str
		Path and name of log file or None.
	debug : bool
		Set debug level, otherwise info. Default: False.
	file_debug : bool
		Same as debug but just for the log file. Default: False
	"""

	# prepare root logger (we do it manually below)
	# logging.basicConfig()

	loglevel = logging.DEBUG if debug else logging.INFO
	root_logger = logging.getLogger()
	root_logger.setLevel(loglevel)

	# console handler
	ch = logging.StreamHandler(sys.stdout)
	ch.setLevel(loglevel)

	# output formatter
	formatter = logging.Formatter('[%(levelname)s] %(asctime)s - %(name)s: %(message)s')
	ch.setFormatter(formatter)
	root_logger.addHandler(ch)

	if filepath is not None:
		fh = logging.FileHandler(filepath)
		fh.setLevel(logging.DEBUG if file_debug else logging.INFO)
		fh.setFormatter(formatter)
		root_logger.addHandler(fh)

	# configure other loggers
	# suse_logger = logging.getLogger('suggestion_service')
	# suse_logger.setLevel(loglevel) # set at different level if desired
	# suse_logger.propagate = False # disable package logging

	return logging.getLogger(name)
