class ArgRange(object):
	""" Allows to restrict arguments like floats to the given range.
	Based on: https://stackoverflow.com/a/59678681
	"""

	def __init__(self, start, end, exclusive=False):
		self.start = start
		self.end = end
		self.exclusive = exclusive

	def __eq__(self, other):
		if self.exclusive: return self.start <= other < self.end
		return self.start <= other <= self.end

	def __contains__(self, item):
		return self.__eq__(item)

	def __iter__(self):
		""" To make usable as choices=Range() instead of choices=[Range()]. """
		yield self

	def __str__(self):
		if self.exclusive: return f'[{self.start}, {self.end})'
		return f'[{self.start}, {self.end}]'



# Method that does things similar to the class above.
# Can be used as the "type" of an argument.
def float_in_range(value, a, b, exclusive=False):
	""" Check if value is of type float and in given range.
	If exclusive is True, use range a >= v < b, otherwise a >= v <= b.
	"""
	try: value = float(value)
	except ValueError: raise argparse.ArgumentTypeError(f'{value} is no float')
	val_below = value < a
	val_above = value >= b if exclusive else value > b
	if val_below or val_above:
		rinfo = f'{a} <= x {"<" if exclusive else "<="} {b}'
		raise argparse.ArgumentTypeError(f'x={value} not in range {rinfo}')
	return value
