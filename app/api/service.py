import flask
import logging
from flask import request
import typing
from enum import Enum


class SuggestionAPI:
	""" Provides a simple endpoint to retrieve suggestions. """

	class SuggestionMethod(Enum):
		ARSR = 'arsr',  # association rule and semantic relatedness (our approach)
		CVR = 'cedar_vr'  # CEDAR value Recommender's scoring approach

	def __init__(self, app: flask.app.Flask, suggestion_method: typing.Callable[[dict, str], dict]):
		"""
		Parameters
		----------
		app
			Instance of the flask app to use.
		suggestion_method
			Method that takes populated_fields and target_fields
			and returns a dict with key 'suggestions' for which
			the value are suggestions from the SuggestionService.
		"""
		self.logger = logging.getLogger(__name__)
		self.app = app
		self.gen_suggestions = suggestion_method
		self.gen_suggestions_cvr = None
		self.test_message = 'pong'
		self.BAD_REQUEST = 400
		self.app.add_url_rule('/ping', view_func=self.endpoint_test)
		self.app.add_url_rule('/suggest', view_func=self.suggest_arsr, methods=['POST'])

	def add_cedar_endpoint(self, suggestion_func: typing.Callable[[dict, str], dict]):
		""" Add endpoint for retrieving Cedar VR recommendations, used for evaluation. """
		self.gen_suggestions_cvr = suggestion_func
		self.app.add_url_rule('/suggest/cedar', view_func=self.suggest_cedar, methods=['POST'])

	def endpoint_test(self):
		return self.test_message

	def suggest_arsr(self):
		""" Use our scoring approach for suggestions. """
		return self.suggest(SuggestionAPI.SuggestionMethod.ARSR)

	def suggest_cedar(self):
		""" Use Cedar Value Recommender scoring approach for suggestions - for evaluation. """
		return self.suggest(SuggestionAPI.SuggestionMethod.CVR)

	def suggest(self, suggestion_method: typing.Union[SuggestionMethod, str]):
		""" Actual function executed when calling suggestion endpoints. """
		jdata = request.get_json(force=True, silent=True)

		if jdata is None:
			return {
				'success': False,
				'reason': 'Invalid JSON data'
			}, self.BAD_REQUEST

		populated_fields = jdata.get('populated_fields')
		target_field = jdata.get('target_field')

		resp = self.check_data(populated_fields, target_field)
		if resp is not None: return resp

		# run suggestion service
		try:
			if suggestion_method == SuggestionAPI.SuggestionMethod.ARSR:
				gen_func = self.gen_suggestions
			elif suggestion_method == SuggestionAPI.SuggestionMethod.CVR:
				gen_func = self.gen_suggestions_cvr
			else:
				raise ValueError('invalid suggestion method')
			result = gen_func(populated_fields, target_field)  # a dict
			if 'suggestions' not in result:
				raise ValueError('result is missing suggestions')
		except Exception as ex:
			self.logger.exception(ex)
			return {
				'success': False,
				'reason': f'Failed to generate suggestions: {str(ex)}'
			}

		resp = {'success': True}
		resp.update(result)  # adds suggestions (and stats)
		return resp

	def check_data(self, populated_fields, target_field):
		""" Checks input data and returns None or tuple for response. """
		if populated_fields is None:
			return {
				'success': False,
				'reason': 'Missing populated_fields.'
			}, self.BAD_REQUEST

		if target_field is None:
			return {
				'success': False,
				'reason': 'Missing target_field.'
			}, self.BAD_REQUEST

		# check basic data types to avoid some invalid requests
		if not isinstance(target_field, str):
			return {
				'success': False,
				'reason': 'Invalid type of target_field.'
			}

		if not isinstance(populated_fields, dict):
			return {
				'success': False,
				'reason': 'Invalid type of populated_fields.'
			}

		return None

	def run(self, host=None, port=None, debug=None, log_requests: bool = None):
		""" Start the flask application server.
		If log_requests is set to False, werkzeug logger will be set to warning level.
		"""
		if log_requests is False:
			print('Disabling request logging of werkzeug.')
			wzlogger = logging.getLogger('werkzeug')
			wzlogger.setLevel(logging.WARNING)
		# https://flask.palletsprojects.com/en/2.0.x/api/#flask.Flask.run
		self.app.run(host=host, port=port, debug=debug)

	@classmethod
	def setup(cls, suggestion_method):
		""" Set up flask app and SuggestionAPI instance.

		Parameters
		----------
		suggestion_method : callable
			Method that takes populated_fields and target_fields
			and returns the suggestions from the SuggestionService.
		"""
		app = flask.Flask(__name__)
		return cls(app, suggestion_method)
