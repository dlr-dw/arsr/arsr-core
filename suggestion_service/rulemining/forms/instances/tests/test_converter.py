import unittest

from .. import FormInstance, TransactionConverter
from ..field import FormField


class TestTransactionConverter(unittest.TestCase):
	""" Test class that converts form instances into transaction strings. """

	def setUp(self):

		self.form_instances = []

		i1 = FormInstance()
		i1.add_field_simple('i1-field1', 'value')
		self.form_instances.append(i1)

		i2 = FormInstance()
		i2.add_field_simple('i2-field1', ['v1', 'v2', 'v3'])
		self.form_instances.append(i2)

		i3 = FormInstance()
		i3.add_field_simple('i3-field1', 'value')
		i3.add_field_simple('i3-field2', ['v1', 'v2', 'v3'])
		self.form_instances.append(i3)

	def test_convert_single(self):
		""" Convert a field with a single value. """
		# FieldValue('value')  # old code for FieldValue
		value = 'value'
		field = FormField('i1-field1', values=value)
		converter = TransactionConverter()
		transaction = converter.convert(self.form_instances[0])  # get tuple
		self.assertEqual(len(transaction), 1)  # a single field-value pair
		self.assertEqual(transaction[0], 0)  # Id of first element is 0
		key = converter.generate_key(field, value)
		data_id = converter.get_dId(key)
		self.assertEqual(data_id, 0)
		dta = converter.get_data(data_id)
		self.assertEqual(dta['field'], field.name)
		# self.assertEqual(dta['value'], value.value)  # old code for FieldValue
		self.assertEqual(dta['value'], value)

	def test_convert_list(self):
		""" Convert a field with multiple values. """
		form_inst = self.form_instances[1]
		converter = TransactionConverter()
		transaction = converter.convert(form_inst)
		self.assertEqual(len(transaction), 3)  # 3 field-value pairs
		self.assertEqual(sorted(transaction), sorted((0, 1, 2)))

		# test retrieving stored data
		field = form_inst.get_field('i2-field1')
		for value in field.values:
			key = converter.generate_key(field, value)
			dta = converter.get_data(converter.get_dId(key))
			self.assertEqual(dta, {'field': field.name, 'value': value})
			# self.assertEqual(dta, {'field': field.name, 'value': value.value}) # old code for FieldValue

	def test_convert_fail(self):
		""" Check that error is raised if passed value is of wrong type. """
		converter = TransactionConverter()
		self.assertRaises(TypeError, converter.convert, 'invalid')

	def test_convert_mixed(self):
		field1 = FormField('i3-field1')
		field2 = FormField('i3-field2')
		converter = TransactionConverter()
		transaction = converter.convert(self.form_instances[2])
		self.assertEqual(len(transaction), 4)  # 4 field-value pairs

		# value = FieldValue('value')  # old code for FieldValue
		value = 'value'
		key = converter.generate_key(field1, value)
		dta = converter.get_data(converter.get_dId(key))
		# self.assertEqual(dta, {'field': field1.name, 'value': value.value})  # old code for FieldValue
		self.assertEqual(dta, {'field': field1.name, 'value': value})

		# value = FieldValue('v1')  # old code for FieldValue
		value = 'v1'
		key = converter.generate_key(field2, value)
		dta = converter.get_data(converter.get_dId(key))
		# self.assertEqual(dta, {'field': field2.name, 'value': value.value})  # old code for FieldValue
		self.assertEqual(dta, {'field': field2.name, 'value': value})

		# value = FieldValue('v2')  # old code for FieldValue
		value = 'v2'
		key = converter.generate_key(field2, value)
		dta = converter.get_data(converter.get_dId(key))
		# self.assertEqual(dta, {'field': field2.name, 'value': value.value})  # old code for FieldValue
		self.assertEqual(dta, {'field': field2.name, 'value': value})

		# value = FieldValue('v3')  # old code for FieldValue
		value = 'v3'
		key = converter.generate_key(field2, value)
		dta = converter.get_data(converter.get_dId(key))
		# self.assertEqual(dta, {'field': field2.name, 'value': value.value})  # old code for FieldValue
		self.assertEqual(dta, {'field': field2.name, 'value': value})

	def test_convert_all(self):
		""" Test using generator to convert multiple instances. """
		converter = TransactionConverter()
		transaction_gen = converter.convert_all(self.form_instances[:2])

		t1 = next(transaction_gen)  # get first transaction
		self.assertEqual(len(t1), 1)  # a single field-value pair

		# we don't validate much here as we did in another test case already
		t2 = next(transaction_gen)  # get second transaction
		self.assertEqual(len(t2), 3)

	def test_field_with_type(self):
		""" Test converting a field with type. """
		instance = FormInstance()
		field = FormField('i1-field1', ['v1', 'v2'], field_type='my-type')
		instance.add_field(field)

		converter = TransactionConverter()
		transaction = converter.convert(instance)
		self.assertEqual(transaction, (0, 1))

		key = converter.generate_key(field, 'v1')
		data_id = converter.get_dId(key)
		data = converter.get_data(data_id)
		self.assertEqual(data, {'field': field.name, 'value': 'v1', 'field_type': field.type})

		key = converter.generate_key(field, 'v2')
		data_id = converter.get_dId(key)
		data = converter.get_data(data_id)
		self.assertEqual(data, {'field': field.name, 'value': 'v2', 'field_type': field.type})
