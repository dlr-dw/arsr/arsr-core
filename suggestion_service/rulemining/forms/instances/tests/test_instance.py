import unittest
import json

from .. import FormInstance, FormField


class TestFormInstance(unittest.TestCase):

	def test_instance_creation(self):
		""" Test FormInstance, FormField, and FieldValue classes. """
		inst = FormInstance()

		# v_cat = FieldValue('cat') # old code for FieldValue
		v_cat = 'cat'
		field1 = FormField('Name', values=[v_cat])

		# v_fur = FieldValue('fur') # old code for FieldValue
		v_fur = 'fur'
		field2 = FormField('Tags', values=[v_fur])
		# field2.add_value(FieldValue('claws'))  # old code for FieldValue
		field2.add_value('claws')

		inst.add_field(field1)
		inst.add_field(field2)

		# adding already added fields
		self.assertFalse(inst.add_field(field2))
		self.assertTrue(inst.add_field(field2, replace=True))

		# check various things like field counts, existence, values
		self.assertEqual(inst.field_count(), 2)
		self.assertIn('name', inst.get_field_names())
		self.assertIn('tags', inst.get_field_names())
		self.assertTrue(inst.has_field('name'))
		self.assertTrue(inst.has_field('tags'))

		# check correct retrieval
		self.assertEqual(inst.get_field('name'), field1)
		self.assertEqual(inst.get_field('tags'), field2)

		# retrieval of stored values (old code for FieldValue)
		# self.assertEqual(v_cat.value, 'cat')
		# self.assertEqual(v_fur.value, 'fur')
		# self.assertTrue(field1.has_value(v_cat.value))
		# self.assertFalse(field1.has_value('cat', 'other type'))

		# is value correctly stored in field
		self.assertTrue(field1.has_value('cat'))
		self.assertTrue(field1.has_value(' cat '))
		self.assertTrue(field1.has_value('Cat'))
		self.assertFalse(field1.has_value('cats'))

		# values stored in field and amount
		self.assertIn(v_cat, field1.values)
		self.assertIn(v_fur, field2.values)
		self.assertEqual(field1.value_count(), 1)
		self.assertEqual(field2.value_count(), 2)
		self.assertEqual(len(field2), field2.value_count())

	def test_from_json(self):
		""" Test instance creation from JSON string and dict. """
		dbr_size = 'http://dbpedia.org/resource/Size'
		instance_dict = {'fields': {
			'name': {
				'values': ['cat']
			},
			'size': {
				'type': dbr_size,
				'values': ['small']
			},
			'tags': {
				'values': ['fur', 'claws', 'whiskers']
			}
		}}

		# make json and then load from it
		json_str = json.dumps(instance_dict)
		i = FormInstance.from_json(json_str)

		# test field presence
		self.assertTrue(i.has_field('name'))
		self.assertTrue(i.has_field('size'))
		self.assertTrue(i.has_field('tags'))

		# test value presence
		self.assertFalse(i.get_field('name').has_value('cats'))
		self.assertTrue(i.get_field('name').has_value('cat'))
		self.assertTrue(i.get_field('size').has_value('small'))
		self.assertEqual(i.get_field('size').type, dbr_size)

		# test direct value access
		name_field = i.get_field('name')
		cat_val = list(name_field.values)[0]
		self.assertEqual(cat_val, 'cat')

		# test field with multiple values
		tags = i.get_field('tags')
		self.assertEqual(tags.value_count(), 3)
		self.assertEqual(len(tags), 3)
		self.assertTrue(tags.has_value('fur'))
		self.assertTrue(tags.has_value('claws'))
		self.assertTrue(tags.has_value('whiskers'))

	def test_legacy_from_dict(self):
		""" Test legacy conversion from dict. """
		inst_legacy = FormInstance.from_dict_legacy({
			'fields': {
				'name': 'Cat',
				'tags': ('fur', 'claws', 'whiskers')
			}
		})

		# current format for the same information
		inst = FormInstance.from_dict({
			'fields': {
				'name': {
					'values': ['Cat']
				},
				'tags': {
					'values': ['fur', 'claws', 'whiskers']
				}
			}
		})

		# check that fields are the same
		self.assertEqual(inst_legacy.field_count(), inst.field_count())
		self.assertEqual(inst_legacy.get_field_names(), inst.get_field_names())

		# test if converted output is same
		legacy_str = json.dumps(inst_legacy.to_dict())
		current_str = json.dumps(inst.to_dict())
		self.assertEqual(legacy_str, current_str)

	@unittest.skip('Test using old code for FieldValue')
	def test_legacy_from_dict_old(self):
		""" Test legacy conversion from dict. """
		inst_legacy = FormInstance.from_dict_legacy({
			'fields': {
				'name': 'Cat',
				'tags': ('fur', 'claws', 'whiskers')
			}
		})

		inst = FormInstance.from_dict({
			'fields': {
				'name': {
					'values': [{'value': 'Cat'}]
				},
				'tags': {
					'values': [
						{'value': 'fur'},
						{'value': 'claws'},
						{'value': 'whiskers'}
					]
				}
			}
		})

		# check that fields are the same
		self.assertEqual(inst_legacy.field_count(), inst.field_count())
		self.assertEqual(inst_legacy.get_field_names(), inst.get_field_names())

		# test if converted output is same
		legacy_str = json.dumps(inst_legacy.to_dict())
		current_str = json.dumps(inst.to_dict())
		self.assertEqual(legacy_str, current_str)

	@unittest.skip('Tests the old code for FieldValue usage (currently no longer used).')
	def test_from_json_old(self):
		""" Test instance creation from JSON string and dict. """

		# before:
		# fields_info = {
		# 	'name': 'cat',
		# 	'type': 'mammal',
		# 	'size': 'small',
		# 	'tags': ('fur', 'claws', 'whiskers')
		# }

		dbr_cat = 'http://dbpedia.org/resource/Cat'
		dbr_size = 'http://dbpedia.org/resource/Size'

		fields_info = {
			'name': {
				'values': [
					{'value': 'cat', 'type': dbr_cat}
				]
			},
			'type': {
				'values': [
					{'value': 'mammal'}
				]
			},
			'size': {
				'type': dbr_size,
				'values': [
					{'value': 'small'}
				]
			},
			'tags': {
				'values': [
					{'value': 'fur'},
					{'value': 'claws'},
					{'value': 'whiskers'}
				]
			}
		}

		instance_dict = {'fields': fields_info}
		json_str = json.dumps(instance_dict)
		i = FormInstance.from_json(json_str)

		# test field presence
		self.assertTrue(i.has_field('name'))
		self.assertTrue(i.has_field('type'))
		self.assertTrue(i.has_field('size'))
		self.assertTrue(i.has_field('tags'))

		# test value presence
		self.assertFalse(i.get_field('name').has_value('cat'))
		self.assertTrue(i.get_field('name').has_value('cat', dbr_cat))
		self.assertTrue(i.get_field('type').has_value('mammal'))
		self.assertTrue(i.get_field('size').has_value('small'))
		self.assertEqual(i.get_field('size').type, dbr_size)

		# test direct value access
		name_field = i.get_field('name')
		cat_val = list(name_field.values)[0]
		self.assertEqual(cat_val.value, 'cat')
		self.assertEqual(cat_val.type, dbr_cat)

		# test field with multiple values
		tags = i.get_field('tags')
		self.assertEqual(tags.value_count(), 3)
		self.assertEqual(len(tags), 3)
		self.assertTrue(tags.has_value('fur'))
		self.assertTrue(tags.has_value('claws'))
		self.assertTrue(tags.has_value('whiskers'))
