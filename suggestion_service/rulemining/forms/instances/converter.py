from .instance import FormInstance
from .field import FormField


class TransactionConverter:
	""" Convert form instances to proper transactions for mining. """

	def __init__(self):
		self.key_to_id = dict()  # map key to id
		self.data = []  # store data (dict) for each id

	def get_data(self, dId):
		""" Get data stored for this id or None. """
		if dId < 0 or dId >= len(self.data): return None
		return self.data[dId]

	def get_dId(self, key):
		""" Get data Id for this key or None. """
		return self.key_to_id.get(key)

	@staticmethod
	def generate_key(field: FormField, value: str):
		""" Generate a key that uniquely identifies this field-value combination. """
		# key_info = [field.name, value.value]  # (old code for FieldValue)
		# if field.type is not None: key_info.append(field.type)
		# if value.type is not None: key_info.append(value.type)

		key_info = [field.name, value]
		if field.type is not None: key_info.append(field.type)
		return tuple(key_info)

	@staticmethod
	def __generate_data(field: FormField, value: str):
		""" Get the data dict that represents information of this field and value. """
		# data = {'field': field.name, 'value': value.value}  # (old code for FieldValue)
		# if field.type is not None: data['field_type'] = field.type
		# if value.type is not None: data['value_type'] = value.type

		data = {'field': field.name, 'value': value}
		if field.type is not None: data['field_type'] = field.type
		return data

	def __get_or_create(self, field: FormField, value: str, additional_data=None):
		""" Get dId if it exists or create and return it. """
		key = self.generate_key(field, value)
		data_id = self.get_dId(key)
		if data_id is None:
			data_id = len(self.data)
			item_data = self.__generate_data(field, value)
			if additional_data is not None:
				item_data.update(additional_data)
			self.data.append(item_data)
			self.key_to_id[key] = data_id
		return data_id

	def convert(self, instance: FormInstance):
		""" Converts a form instance to a transaction and returns it.

		For field with a set of values, each field-value combination
		will be added as a single entry in the transaction.

		Parameters
		----------
		instance : FormInstance
			The form instance to convert.

		Raises
		------
		ValueError
			If the passed instance is no FormInstance.

		Returns
		-------
		tuple
			Transaction of Ids that represent field-value combinations.
			These Ids are used as items and must be converted back to actual
			RuleItems afterwards, gathering values that belong to each field.
			I.e. the backwards-conversion must be done on an itemset basis,
			i.e. on lhs and rhs of a rule respectively.
		"""
		if not isinstance(instance, FormInstance):
			raise TypeError('Parameter instance must be of type FormInstance.')
		transaction = []
		for name, field in instance.get_fields():
			for v in field.values:
				data_id = self.__get_or_create(field, v)
				transaction.append(data_id)
		return tuple(transaction)

	def convert_all(self, instances):
		""" Convert a list of form instances to transactions.

		Parameters
		----------
		instances : typing.Iterable[FormInstance]
			An iterable of FormInstance objects.

		Raises
		------
		ValueError
			If an instance is of wrong type.

		Returns
		-------
		typing.Generator[tuple[int]]
			A generator to retrieve transactions (tuples of integers).
		"""
		for fi in instances: yield self.convert(fi)
