# This class is currently no longer used (04.2022).
# We previously used it to be able to provide a type for a field value
# but the final structure made the code just more complicated and was not required for the evaluation.
# At the moment, we just use simple strings for the values, as we require URIs anyway.
# Anyhow, this was working and could be reintegrated if required.

class FieldValue:
	""" Class representing a value of a field. """

	def __init__(self, value: str, value_type: str = None):
		self._value = str(value)
		self._type = value_type

	@property
	def value(self):
		""" The actual value stored. """
		return self._value

	@property
	def type(self):
		""" The type of this value (e.g. RDF URI). """
		return self._type

	@staticmethod
	def identifier(value: str, value_type: str = None):
		""" Get identify based on case-folded value and type. """
		return hash((value.casefold(), value_type))

	def __eq__(self, other):
		""" To support comparison with instances of this class. """
		if not isinstance(other, type(self)):
			print('other type')
			return False
		# value and type should match
		value_match = self.value.casefold() == other.value.casefold()
		type_match = self.type == other.type
		# TODO: consider using "or" if type is None instead of "and"
		return value_match and type_match

	def __hash__(self):
		""" To support hashing instances of this class. """
		# Hash collisions with a tuple of same information
		# do not occur because python also checks __eq__.
		return self.identifier(self.value, self.type)

	def to_dict(self):
		""" Converts this instance to dict and returns it. """
		fv_dict = {'value': self.value}
		if self.type is not None:
			fv_dict['type'] = self.type
		return fv_dict

	@classmethod
	def from_dict(cls, d):
		""" Create instance from dict with at least "value" defined.

		Its structure should look like this:
		{
			"value": "actual value",
			"type (optional)": "URI"
		}
		"""
		value = d['value']  # can raise KeyError
		vtype = d['type'] if 'type' in d else None
		if len(d) == 0: return cls(value, vtype)
		return cls(value, value_type=vtype)
