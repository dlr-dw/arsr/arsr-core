import json
import logging
import typing

from .field import FormField


class FormInstance:
	""" Class that represents a single form instance.
	This class is used by form repositories and the transaction converter.
	Repositories load and store it and the converter creates transactions from it.
	"""

	logger = logging.getLogger(__name__)

	def __init__(self):
		# each field takes care of adding each value just once
		self.form = dict()

	def add_field(self, field: FormField, replace=False):
		""" Add a form field to this instance.

		Parameters
		----------
		field : FormField
			An instance of the FormField to add.
			Its name is used as its identifier.
		replace : bool, optional
			If the field already exists, allow to replace it.
			Ultimately, this will replace the field's values.

		Returns
		-------
		bool
			True if the field was added, False otherwise.

		Raises
		------
		TypeError
			If the passed field has invalid type (not FormField).
		"""
		if not isinstance(field, FormField):
			raise TypeError('field has invalid type')
		field_name = field.name.casefold()
		if replace or field_name not in self.form:
			self.form[field_name] = field
			return True
		return False

	def add_field_simple(
		self, name: str, values_str: typing.Union[str, typing.List[str]], field_type: str = None, replace=False):
		""" See FormField.create_simple for more information. """
		self.add_field(FormField.create_simple(name, field_type=field_type, values_str=values_str), replace=replace)

	def get_field(self, name: str, default=None) -> FormField:
		""" Get the FormField by (case folded) name or default. """
		return self.form.get(str(name).casefold(), default)

	def get_fields(self):
		""" Get generator over (case-folded field name, FormField) tuples. """
		return ((name, field) for name, field in self.form.items())

	def get_field_names(self):
		""" Get all the (case-folded) field names used for identification. """
		return self.form.keys()

	def has_field(self, name: str):
		""" Returns True if a field for this (case folded) name exists. """
		return str(name).casefold() in self.form

	def field_count(self):
		""" Get number of form fields. """
		return len(self.form)

	def __str__(self):
		""" Representation when printing an instance. """
		return f'Instance({", ".join(self.form.keys())})'

	def to_dict(self):
		""" Converts instance to dict and returns it.

		Each instance looks like this (see to_dict of FormField and FieldValue):
		{
			"fields": {
				"field1_name": {
					"type": "URI",
					"values": [
						{
							"value": "actual value",
							"type (optional)": URI"
						}
					]
				}
			}
		}
		"""
		jdata = {
			field.name: field.to_dict()
			for field in self.form.values()
		}
		return {
			'fields': jdata
		}

	@classmethod
	def from_dict(cls, d):
		""" Create a FormInstance based on given dict.

		The information of the dict should look like the following.
		We use simple strings for values, which are usually URIs.
		{
			"fields": {
				"field1_name": {
					"type (optional): "URI",
					"values": ["v1", "v2", ...]
				},
				"field2_name": { ... },
				...
			}
		}

		Another possible structure could be the following,
		but we discarded it for now for simplicity.
		{
			"fields": {
				"field1_name": {
					"type (optional): "URI",
					"values": [
						{
							"value": "actual value",
							"type (optional)": "URI"
						}
					]
				},
				"field2_name": { ... },
				...
			}
		}

		Parameters
		----------
		d : dict

		Returns
		-------
		FormInstance
			An instance of the FormInstance class.
		"""
		inst = cls()
		fields = d.get('fields', dict())  # if not available, treat as empty dict
		for field_name, field_info in fields.items():
			field = FormField.from_dict(field_name, field_info)  # raises exception on failure
			added = inst.add_field(field)
			if not added:
				cls.logger.warning(f'Skipped adding field "{field_name}": already exists.')
		return inst

	@classmethod
	def from_json(cls, json_str: str):
		return cls.from_dict(json.loads(json_str))

	@classmethod
	def from_dict_legacy(cls, d):
		""" Load from dict in legacy format.

		The information of the dict should look like this:
		{
			"fields": {
				"field1": "v1",
				"field2": ["v1", "v2", ...],
				...
			}
		}

		Parameters
		----------
		d : dict

		Returns
		-------
		FormInstance
			An instance of the FormInstance class.
		"""
		inst = cls()
		fields = d.get('fields', dict())  # if not available, treat as empty dict
		for field_name, field_value in fields.items():
			values = []
			if isinstance(field_value, str):
				# values.append(FieldValue(field_value))
				values.append(field_value)
			else:
				for v in field_value:
					if not isinstance(v, str): raise TypeError(f'Only str is supported, got: {v}')
					# values.append(FieldValue(v))
					values.append(v)
			field = FormField(field_name, values=values)
			added = inst.add_field(field)
			if not added:
				cls.logger.warning(f'Skipped adding field "{field_name}": already exists.')
		return inst
