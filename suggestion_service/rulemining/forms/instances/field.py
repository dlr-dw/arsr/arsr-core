import typing


class FormField:
	""" Represents a field of a form with string values (usually URIs). """

	def __init__(self, name, values=None, field_type=None):
		"""
		Parameters
		----------
		name : str
			Name (label) of this field.
		values : str | typing.Iterable[str] | None
			A value or list of values (usually URIs).
		field_type : str | None
			An optional type (URI) for this field.
			Could be used to identify fields with different name but same concept.
		"""
		self._name = str(name)
		self._type = field_type
		self._values_added = set()  # for existence check
		self._values = []  # actual values

		if values is not None:
			# if isinstance(values, FieldValue): self.add_value(values)  # (old code for FieldValue)
			if isinstance(values, str): self.add_value(values)
			else: self.add_values(values)

	@property
	def name(self):
		return self._name

	@property
	def type(self):
		return self._type

	@property
	def values(self):
		return self._values

	def get_value(self):
		""" Get the value if there is only one. """
		if len(self._values) != 1: raise ValueError('there is no single value')
		return self.values[0]

	@staticmethod
	def _normalize_value(value: str):
		""" Returns the value normalized. """
		return value.strip().casefold()

	def add_value(self, value: str):
		""" Add a FieldValue if it does not exist yet. """
		if not isinstance(value, str):
			raise TypeError(f'value has invalid type: {type(value)}')
		# value_hash = hash(value)  # (old code for FieldValue)
		normalized_value = self._normalize_value(value)
		if normalized_value in self._values_added: return False
		self._values_added.add(normalized_value)
		self._values.append(value)
		return True

	def add_values(self, values: typing.Iterable[str]):
		""" Add multiple FieldValue's. """
		for v in values: self.add_value(v)

	def has_value(self, value: str):
		""" Check if a FieldValue exists. """
		# value_hash = FieldValue.identifier(value, value_type)
		return self._normalize_value(value) in self._values_added

	def value_count(self):
		""" Returns amount of values stored for this field. """
		return len(self)

	def __len__(self):
		return len(self._values)

	def __str__(self):
		""" String conversion of a FormField instance. """
		# values_str = ', '.join(str(v.value) for v in self._values)
		values_str = ', '.join(self._values)
		type_str = f' ({self._type})' if self._type is not None else ''
		return f'{self._name}{type_str}: [{values_str}]'

	def __eq__(self, other):
		""" Comparison of FormField instances, based on name and type. """
		if not isinstance(other, type(self)): return False
		same_name = self.name.casefold() == other.name.casefold()
		same_type = self.type == other.type
		return same_name and same_type

	def __hash__(self):
		""" Allow hashing instances of this class. """
		return hash((self._name.casefold(), self._type))

	def to_dict(self):
		""" Converts this instance to dict and returns it. """
		d = dict()
		if self.type is not None: d['type'] = self.type
		d['values'] = self.values
		# d['values'] = [field_value.to_dict() for field_value in self.values]  # (old code for FieldValue)
		return d

	@classmethod
	def from_dict(cls, field_name: str, d: dict):
		""" Create FormField instance from dictionary.

		Its structure should look like this:
		{
			"type (optional): "URI",
			"values": ["v1", "v2"]
		}
		"""
		if field_name is None or len(field_name) < 1:
			raise ValueError('missing field name')
		field = cls(field_name, values=d.get('values'), field_type=d.get('type'))
		# add values to field instance (old code for FieldValue)
		# for value_info in d.get('values', []):
		# 	value = FieldValue.from_dict(value_info)
		# 	field.add_value(value)
		return field

	@classmethod
	def create_simple(
		cls, field_name: str, values_str: typing.Union[str, typing.List[str]], field_type: str = None):
		""" Creates a form field instance for this field name with simple string value(s). """
		values = []
		if isinstance(values_str, str): values_str = [values_str]
		for v in values_str:
			if not isinstance(v, str): raise TypeError(f'value {v} is no string')
			# values.append(FieldValue(v))  # (old code for FieldValue)
			values.append(v)
		return cls(field_name, values=values, field_type=field_type)
