from . import FormRepo
from ..instances import FormInstance


class InMemoryFormRepo(FormRepo):
	""" Simulates a form repository by using in-memory data. """

	def __init__(self, **kwargs):
		super().__init__()
		self.instances = []
		if kwargs.get('example') == 'animals':
			self.add_instances(self._animal_instances())

	def add_instance(self, instance):
		self.instances.append(instance)

	def add_instances(self, instances):
		self.instances.extend(instances)

	def get_instances(self):
		""" Generator that returns stored instances. """
		for inst in self.instances: yield inst

	def is_empty(self):
		return len(self.instances) < 1

	@classmethod
	def example_animals(cls):
		""" Creates instance of a InMemoryFormRepo and add animal examples. """
		return cls(example='animals')

	@staticmethod
	def _animal_instances():
		""" Returns a tuple of 10 instances for animal example. """
		return (
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Cat', 'type': 'mammal', 'fly': 'no',
				'tags': ('fur', 'claws', 'whiskers')
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Dog', 'type': 'mammal', 'fly': 'no',
				'tags': ('fur', 'claws', 'whiskers', 'bark')
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Bat', 'type': 'mammal', 'fly': 'yes',
				'tags': ('wings', 'claws')
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Fox', 'type': 'mammal', 'fly': 'no',
				'tags': ('growl', 'whiskers')
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Elephant', 'type': 'mammal', 'fly': 'no',
				'tags': ('large', 'trunk', 'tusks', 'trumpets')
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Duck', 'type': 'bird', 'fly': 'yes',
				'tags': ('wings', 'feathers')
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Penguin', 'type': 'bird', 'fly': 'no',
				'tags': ('wings',)
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Owl', 'type': 'bird', 'fly': 'yes',
				'tags': ('wings', 'talons', 'beak')
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Shark', 'type': 'fish', 'fly': 'no',
				'tags': ('teeth', 'hunt')
			}}),
			FormInstance.from_dict_legacy({'fields': {
				'name': 'Tuna', 'type': 'fish', 'fly': 'no',
				'tags': ('food', 'fast')
			}}),
		)
