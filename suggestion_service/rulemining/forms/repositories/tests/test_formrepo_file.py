from io import StringIO
import unittest
import json

from ...instances import FormInstance
from .. import FileFormRepo


class TestFileFormRepo(unittest.TestCase):
	""" Tests some methods of file form repository.
	Uses a strategy to avoid writing to disk as suggested here:
	https://stackoverflow.com/a/3945057
	"""

	def test_json_io(self):
		""" Test JSON IO without actually writing to disk. """
		repo = FileFormRepo(None, load_now=False, save_on_close=False)

		# This is the currently used format with simple strings as values.
		inst_dict = {
			'fields': {
				'field1': {
					'values': ['value1']
				},
				'field2': {
					'values': ['v21', 'v22', 'v23']
				}
			}
		}

		instances_dict = {'instances': []}
		instances_dict['instances'].append(inst_dict)

		# load from simulated file
		file_in = StringIO(json.dumps(instances_dict))
		repo._load_file(file_in)

		# add another instance
		new_inst_dict2 = {
			'fields': {
				'field3': {
					'values': ['value3']
				},
				'field4': {
					'values': ['value4']
				}
			}
		}
		instances_dict['instances'].append(new_inst_dict2)
		repo.add_instance(FormInstance.from_dict(new_inst_dict2))

		# now write to a simulated file
		file_out = StringIO()
		repo._write_file(file_out, indent=None)

		# compare the input JSON string to the output JSON string
		result_str = file_out.getvalue()
		expected_str = json.dumps(instances_dict, indent=None)
		self.assertEqual(result_str, expected_str)

	@unittest.skip('Tests a format using old code for FieldValue.')
	def test_json_io_old(self):
		""" Test JSON IO without actually writing to disk. """
		repo = FileFormRepo(None, load_now=False, save_on_close=False)

		instances_dict = {'instances': []}
		inst_dict = {
			'fields': {
				'field1': {
					'values': [{'value': 'value1'}]
				},
				'field2': {
					'values': [
						{'value': 'v21'},
						{'value': 'v22'},
						{'value': 'v23'}
					]
				}
			}
		}
		instances_dict['instances'].append(inst_dict)

		# load from simulated file
		file_in = StringIO(json.dumps(instances_dict))
		repo._load_file(file_in)

		# add another instance
		new_inst_dict2 = {
			'fields': {
				'field3': {
					'values': [{'value': 'value3'}]
				},
				'field4': {
					'values': [{'value': 'value4'}]
				}
			}
		}
		instances_dict['instances'].append(new_inst_dict2)
		repo.add_instance(FormInstance.from_dict(new_inst_dict2))

		# now write to a simulated file
		file_out = StringIO()
		repo._write_file(file_out, indent=None)

		# compare the input JSON string to the output JSON string
		result_str = file_out.getvalue()
		expected_str = json.dumps(instances_dict, indent=None)
		self.assertEqual(result_str, expected_str)
