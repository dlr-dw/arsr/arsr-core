import json
from .formrepo import FormRepo
from ..instances import FormInstance
from suggestion_service.io import IOUtils


class FileFormRepo(FormRepo):
	""" Simulates a form repository by loading data from a file.
	This data is stored in-memory the same way as for the InMemoryFormRepo.
	"""

	def __init__(self, filepath, load_now=True, save_on_close=True, legacy_data=False):
		super().__init__()
		self.instances = []
		self.filepath = filepath
		self.save_on_close = save_on_close
		self.legacy_data = legacy_data
		if load_now: self._load(filepath)

	def add_instance(self, instance):
		self.instances.append(instance)

	def add_instances(self, instances):
		self.instances.extend(instances)

	def get_instances(self):
		""" Generator that returns stored instances. """
		for inst in self.instances: yield inst

	def is_empty(self):
		return len(self.instances) == 0

	def close(self):
		if self.save_on_close: self._write(self.filepath, indent=2)

	def _load(self, filepath, legacy_forms=False):
		""" Load instances from JSON file given by path. """
		print(f'Loading instances from file: {filepath}')
		with open(filepath, 'r') as fin:
			loaded = self._load_file(fin, legacy_forms=legacy_forms)
		print(f'Instances loaded: {loaded}')
		return loaded

	def _load_file(self, file_handle, legacy_forms=False):
		""" Load instances from JSON file handle.

		The file format must look as follows:
		{
			"instances": [
				{
					"fields": {
						"field1": {
							"type (optional)": "URI",
							"values": ["v1", "v2", ...]
						}
						"field2": {
							...
						}
					}
				},
				...
			}
		}

		The inner part of each instance uses the same conversion format
		as used by from_dict() function of the class FormInstance.
		We currently only use strings as values for simplicity.
		Otherwise, a value looked like this: {"value": "actual value", "type": "URI"}.

		Raises
		------
		json.decoder.JSONDecodeError
			If the JSON to load is invalid.
		ValueError
			If no instances were found in the file.

		Returns
		-------
		int
			Number of instances loaded.
		"""
		if isinstance(file_handle, str): raise ValueError('Invalid handle!')
		jdata = json.load(file_handle)

		instances = jdata.get('instances')
		if instances is None:
			raise ValueError(f'No instances found in file: {file_handle.name}')

		# clear currently stored data and load new
		self.instances.clear()
		loaded = 0
		for inst_dict in instances:
			if legacy_forms or self.legacy_data:
				instance = FormInstance.from_dict_legacy(inst_dict)
			else:
				instance = FormInstance.from_dict(inst_dict)
			if instance.field_count() < 1: continue  # skip
			self.instances.append(instance)
			loaded += 1
		return loaded

	def _write(self, filepath, **kwargs):
		""" Write stored instances to file as JSON.

		Parameters
		----------
		filepath : str
			Name of file or path to file including ".json" extension.
		"""
		path = IOUtils.prepare_file_write(filepath)
		if path is None: raise ValueError(f'Invalid file: {filepath}')
		print(f'Writing instances to: {path.resolve()}')
		with path.open('w') as fout:
			self._write_file(fout, **kwargs)

	def _write_file(self, file_handle, **kwargs):
		""" Write the currently stored instances to the file.

		Parameters
		----------
		file_handle
			File handle used to write to (e.g. created with open()).
		**kwargs
			See available parameters of json.dump() method:
			https://docs.python.org/3/library/json.html#json.dump
		"""
		if isinstance(file_handle, str): raise ValueError('Invalid handle!')

		jdata = {
			'instances': list(i.to_dict() for i in self.get_instances())
		}

		json.dump(jdata, file_handle, **kwargs)
