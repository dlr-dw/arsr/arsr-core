class FormRepo:
	""" Base class for form repository connections. """

	def __init__(self):
		pass

	def add_instance(self, instance):
		pass

	def add_instances(self, instances):
		pass

	def get_instances(self):
		""" Returns an iterator over instances. """
		pass

	def is_empty(self):
		""" Returns True if the repository has no instances. """
		pass

	def close(self):
		""" Close connections and free resources if there are any. """
		pass
