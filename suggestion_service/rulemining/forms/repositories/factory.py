from . import FileFormRepo, InMemoryFormRepo


class FormRepoFactory:
	""" Factory to instantiate available repositories. """

	@classmethod
	def create(cls, repo, **kwargs):
		""" Create an instance of a repository with given settings.

		Parameters
		----------
		repo : str
			Name of the repository to create an instance of (case insensitive).
		**kwargs
			Arguments to pass as settings to the constructors of repositories.

		Returns
		-------
		FormRepo
			An instance of the according form repository.

		Raises
		------
		ValueError
			If there is no such repository for the given name.
		"""

		repo = repo.lower().strip()
		if repo == 'file': return FileFormRepo(**kwargs)
		if repo == 'in-memory': return InMemoryFormRepo(**kwargs)
		raise ValueError('Invalid form repository!')
