# third party
from efficient_apriori import itemsets_from_transactions, generate_rules_apriori

# local
from . import RuleMiningMethod


class Apriori(RuleMiningMethod):

	def __init__(self, minsupp=0.5, minconf=0.5, max_length=8, verbosity=0):
		""" Apriori algorithm based on "efficient_apriori" library.

		Parameters
		----------
		minsupp : float
			Minimal support (frequency of item co-occurrences) in range 0-1.
		minconf : float
			Minimal confidence of rules in range 0-1.
		max_length : int
			Maximum length k of k-itemsets and rules.
			Should be the maximum length of all transactions we have.
		"""
		super().__init__()
		self.minsupp = minsupp
		self.minconf = minconf
		self.max_length = max_length # max length of itemsets and rules
		self.verbosity = verbosity # 0, 1 or 2 (print detail)

	def mine(self, transactions):
		""" Mine association rules from given transactions.

		Parameters
		----------
		transactions : typing.Iterable[typing.Union[set, tuple, list]]
			Form instances that have been converted to transactions.

		Returns
		-------
		generator of dicts
			Yields a rule dict consisting of: lhs, rhs, support, confidence
		"""
		# We can't convert rules on the fly by using the single function.
		# Therefore, we use the two separate functions that "yield" the rules.
		# Regular usage: itemsets, rules = apriori(transactions, minsupp, minconf)

		# Usage: https://github.com/tommyod/Efficient-Apriori/blob/96e89176b48a8255010c2951968894c22a1897e8/efficient_apriori/apriori.py#L12
		output_transaction_ids = False # we don't need this, so disable it
		itemsets, num_trans = itemsets_from_transactions(transactions,
			self.minsupp, self.max_length, self.verbosity, output_transaction_ids)

		# create dict of dicts {length: {item: count}}
		# e.g. {1: {('A',): 2, ('B',): 3, ...}, 2: {....}, 3: {...}}
		# (as many "length" entries as max_length or k-itemsets exist)
		# slightly modified version of the code from github (we don't use
		# counter.itemset_count, as counter is already an int for us - dont know why)
		itemsets_raw = {
			length: {
				#item: counter.itemset_count # original
				item: counter # modified
				for (item, counter) in itemsets.items()
			}
			for (length, itemsets) in itemsets.items()
		}

		# generate actual rules from itemsets
		# Rule class: https://github.com/tommyod/Efficient-Apriori/blob/96e89176b48a8255010c2951968894c22a1897e8/efficient_apriori/rules.py#L13
		rules_gen = generate_rules_apriori(itemsets_raw,
			self.minconf, num_trans, self.verbosity)

		for r in rules_gen:
			yield {
				'lhs': r.lhs,
				'rhs': r.rhs,
				'support': r.support,
				'confidence': r.confidence
			}
