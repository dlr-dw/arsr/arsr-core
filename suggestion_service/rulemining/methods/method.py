class RuleMiningMethod:
	""" Base class for rule mining methods. """

	def __init__(self):
		pass

	def mine(self, transactions):
		""" Mine association rules from given transactions (list of tuples of strings).

		Parameters
		----------
		transactions : typing.Iterable | typing.Collection
			Form instances that have been converted to transactions.

		Returns
		-------
		generator of dicts
			Yields a rule dict consisting of: lhs, rhs, support, confidence
		"""
		pass
