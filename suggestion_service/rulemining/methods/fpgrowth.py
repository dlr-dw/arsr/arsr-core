from . import RuleMiningMethod


class FPGrowth(RuleMiningMethod):

	def __init__(self, **kwargs):
		super().__init__()
		raise NotImplementedError('FP growth is not yet implemented.')

	def mine(self, transactions):
		""" Mine association rules from given transactions. """
		pass
