from . import Apriori, FPGrowth


class MethodFactory:
	""" Factory to instantiate available methods. """

	@classmethod
	def create(cls, method, **kwargs):
		""" Create an instance of a method with given settings.

		Parameters
		----------
		method : str
			Name of the method to create an instance of (case insensitive).
		**kwargs
			Arguments to pass as settings to the constructors of methods.

		Returns
		-------
		RuleMiningMethod
			An instance of the according method.

		Raises
		------
		ValueError
			If there is no such method for the given name.
		"""

		method = method.lower().strip()

		if method == 'apriori':
			return Apriori(**kwargs)

		if method == 'fpgrowth':
			return FPGrowth(**kwargs)

		raise ValueError('Invalid method!')
