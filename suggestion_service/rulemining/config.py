import json

from .forms.repositories import FormRepoFactory
from .rules.repositories import RuleRepoFactory
from .methods import MethodFactory


class RuleMiningConfig:
	""" Configuration for a rule mining service execution. """

	def __init__(self, path):
		""" Reads the JSON config to make settings available. """
		self._formrepo = None
		self._rulerepo = None
		self._method = None
		self._verbosity = 0
		self._load_file(path)

	@property
	def formrepo(self): return self._formrepo

	@property
	def rulerepo(self): return self._rulerepo

	@property
	def method(self): return self._method

	@property
	def verbosity(self): return self._verbosity

	def _load_file(self, path):
		with open(path, 'r') as fin:
			self._load_config(fin)

	def _load_config(self, file_handle):
		""" Loads config from file contents. """
		jdata = json.load(file_handle)
		# TODO: schema validation

		confname = jdata.get('name')
		if confname is not None:
			print(f'Loading rule mining config: {confname}')

		c_main = jdata.get('config')
		self._verbosity = c_main.get('verbosity', self._verbosity)

		c_formrepo = c_main.get('formrepo')
		self._formrepo = FormRepoFactory.create(
			c_formrepo.get('name'),
			**c_formrepo.get('config', {})
		)

		c_method = c_main.get('method')
		self._method = MethodFactory.create(
			c_method.get('name'),
			**c_method.get('config', {})
		)

		c_rulerepo = c_main.get('rulerepo')
		self._rulerepo = RuleRepoFactory.create(
			c_rulerepo.get('name'),
			**c_rulerepo.get('config', {})
		)
