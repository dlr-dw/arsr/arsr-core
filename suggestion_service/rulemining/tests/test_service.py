import unittest

from .. import RuleMiningService, forms, rules, methods


class TestRuleMiningService(unittest.TestCase):
	""" Tests for the Rule Mining Service. """

	def test_basic(self):
		""" Test basic workflow of RMS. """

		# prepare data to mine from
		formrepo = forms.repositories.InMemoryFormRepo()
		formrepo.add_instances([
			forms.instances.FormInstance.from_dict_legacy({'fields': {
				'A': 'a1', 'B': 'b1'
			}}),
			forms.instances.FormInstance.from_dict_legacy({'fields': {
				'A': 'a1', 'B': 'b1', 'C': 'c1'
			}}),
			forms.instances.FormInstance.from_dict_legacy({'fields': {
				'A': 'a2', 'B': 'b2', 'C': 'c2'
			}})
		])

		# prepare method, storage and service
		mining_method = methods.Apriori(minsupp=0.5, minconf=1)
		rulerepo = rules.repositories.InMemoryRuleRepo()
		service = RuleMiningService()

		# start mining process (stores rules in rulerepo)
		service.generate_rules(formrepo, mining_method, rulerepo)

		# should generate two rules (a1 -> b1 and b1 -> a1)
		rules_str = []
		for rule in rulerepo.get_rules():
			lhs = ','.join(f'[{",".join(item.values)}]' for item in rule.antecedent())
			rhs = ','.join(f'[{",".join(item.values)}]' for item in rule.consequent())
			rules_str.append(f'{lhs} -> {rhs}')

		self.assertIn('[a1] -> [b1]', rules_str)
		self.assertIn('[b1] -> [a1]', rules_str)
