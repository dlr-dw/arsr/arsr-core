import logging
from . import methods, forms, rules


class RuleMiningService:
	""" Main component of rule mining module. """

	def __init__(self, verbosity=0):
		self.logger = logging.getLogger(__name__)
		self.verbosity = verbosity

	def generate_rules(self, formrepo, method, rulerepo):
		""" Generates association rules and stores them in rulerepo.

		Parameters
		----------
		formrepo : FormRepo
			The form repository to retrieve data from.
		method : RuleMiningMethod
			Instance of a rule mining method to use.
			Use the MethodFactory class for instantiation.
		rulerepo : RuleRepo
			The rule repository used to store rules.

		Raises
		------
		ValueError
			1. If formrepo is not an instance of FormRepo.
			2. If the rule mining method is invalid.
			3. If rulerepo is not an instance of RuleRepo
		"""
		if not isinstance(formrepo, forms.repositories.FormRepo):
			raise ValueError('invalid form repository')
		if not isinstance(method, methods.RuleMiningMethod):
			raise ValueError('invalid method')
		if not isinstance(rulerepo, rules.repositories.RuleRepo):
			raise ValueError('invalid rule repository')

		# Get instances (generator) from repository
		instances_gen = formrepo.get_instances()

		# check if there are any transactions
		# (not required in general but apriori impl. fails in this case)
		if formrepo.is_empty():
			self.logger.info('No instances in form repository.')
			return

		# Convert instances to transactions
		converter = forms.instances.TransactionConverter()
		transactions_gen = converter.convert_all(instances_gen)

		# TODO: maybe it would be safer to check for empty transactions here
		# would require a workaround to see if transactions_gen has any item

		# DEBUG PRINT (decreases performance!)
		if self.verbosity > 0:
			instances_gen_debug = formrepo.get_instances()
			converter_debug = forms.instances.TransactionConverter()
			transactions_gen_debug = converter_debug.convert_all(instances_gen_debug)
			print('\nTransactions:')
			for t in transactions_gen_debug:
				print(f'{t} = {list(converter_debug.get_data(dId) for dId in t)}')

		# create Rule instances and store in RuleRepo
		self.logger.info('Starting rule mining.')
		for rule in method.mine(transactions_gen):
			lhs_items = (converter.get_data(dId) for dId in rule['lhs'])
			lhs = rules.Rule.decode_itemset(lhs_items)
			rhs_items = (converter.get_data(dId) for dId in rule['rhs'])
			rhs = rules.Rule.decode_itemset(rhs_items)
			rule = rules.Rule(lhs, rhs, rule['support'], rule['confidence'])
			# TODO: consider adding rules in batches
			rulerepo.add_rule(rule)

		self.logger.info('Rule mining finished.')
