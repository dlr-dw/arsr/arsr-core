import unittest

from ..rule import RuleItem, Rule


class TestRuleItem(unittest.TestCase):
	""" Test the RuleItem class. """

	def test_rule_item(self):
		""" Test creation of rule items. """
		fieldname = 'afield1'
		values = ['avalue11', 'avalue12']
		item = RuleItem(fieldname, values)
		self.assertEqual(item.field, 'afield1')
		self.assertEqual(tuple(item.values), tuple(values))

	def test_from_dict(self):
		""" Test creation of rule item from dict. """
		fieldname = 'afield1'
		values = ['avalue11', 'avalue12']
		item_dict = {
			RuleItem.FIELD_KEY: fieldname,
			RuleItem.VALUE_KEY: values
		}
		item = RuleItem.from_dict(item_dict)
		self.assertEqual(type(item), RuleItem)
		self.assertEqual(item.field, 'afield1')
		self.assertEqual(tuple(item.values), tuple(values))


class TestRule(unittest.TestCase):
	""" Contains tests for the Rule class. """

	def setUp(self):
		self.r1_antecedent = (
			RuleItem('afield1', ['avalue11', 'avalue12']),
			RuleItem('afield2', 'avalue2'),
		)

		self.r1_consequent = (
			RuleItem('cfield1', 'cvalue1'),
		)

		self.r1 = Rule(
			self.r1_antecedent, self.r1_consequent,
			0.3,  # arbitrary support value
			0.5  # arbitrary confidence value
		)

	def test_rule_access(self):
		""" Test that values are returned properly. """
		self.assertEqual(self.r1.antecedent(), self.r1_antecedent)
		self.assertEqual(self.r1.consequent(), self.r1_consequent)
		self.assertEqual(self.r1.support(), 0.3)
		self.assertEqual(self.r1.confidence(), 0.5)
		self.assertEqual(self.r1.antecedent_count(), len(self.r1_antecedent))
		self.assertEqual(self.r1.consequent_count(), len(self.r1_consequent))
		self.assertEqual(sorted(tuple(self.r1.antecedent_fields())), sorted(('afield1', 'afield2',)))
		self.assertEqual(sorted(tuple(self.r1.consequent_fields())), sorted(('cfield1',)))

	def test_decode_itemset(self):
		""" Test convertion of itemsets to dict. """
		def item(field, value):
			return {'field': field, 'value': value}

		itemset = (
			item('field1', 'value1'),
			item('field2', 'value21'),
			item('field2', 'value22'),
			item('field3', 'value3'),
		)

		expected = (
			RuleItem('field1', 'value1'),
			RuleItem('field2', ['value21', 'value22']),
			RuleItem('field3', 'value3'),
		)

		result = Rule.decode_itemset(itemset)
		self.assertEqual(result, expected)
		self.assertEqual(result[0].values, expected[0].values)
		self.assertEqual(result[2].values, expected[2].values)

		f2 = result[1].values
		self.assertEqual(len(f2), 2)
		self.assertIn('value21', f2)
		self.assertIn('value22', f2)

	def test_decode_with_fieldtype(self):
		""" Test decoding itemset with field type. """
		def item(field, value, field_type=None):
			if field_type is None: return {'field': field, 'value': value}
			return {'field': field, 'value': value, 'field_type': field_type}

		itemset = (
			item('field1', 'v1'),
			item('field2', 'v21', 'http://example.com/Field2'),
			item('field2', 'v22', 'http://example.com/Field2'),
			item('field3', 'v3', 'http://example.com/Field3'),
		)

		expected = (
			RuleItem('field1', 'v1'),
			RuleItem('field2', ['v21', 'v22'], field_type='http://example.com/Field2'),
			RuleItem('field3', 'v3', field_type='http://example.com/Field3'),
		)

		result = Rule.decode_itemset(itemset)
		self.assertEqual(result, expected)
		self.assertEqual(result[0].field_type, None)
		self.assertEqual(result[1].field_type, 'http://example.com/Field2')
		self.assertEqual(result[2].field_type, 'http://example.com/Field3')

	def test_rule_from_dict(self):
		""" Test instantiating a rule from a dict. """
		antecedent = [  # list should work
			{RuleItem.FIELD_KEY: 'f1', RuleItem.VALUE_KEY: 'v1'},
			{RuleItem.FIELD_KEY: 'f2', RuleItem.VALUE_KEY: 'v2'},
			{RuleItem.FIELD_KEY: 'f3', RuleItem.VALUE_KEY: 'v3'},
		]

		consequent = (  # tuple should work as well
			{RuleItem.FIELD_KEY: 'f4', RuleItem.VALUE_KEY: 'v4'},
			{RuleItem.FIELD_KEY: 'f5', RuleItem.VALUE_KEY: 'v5'},
		)

		support = 0.5
		confidence = '0.8'  # str should work as well

		rule = Rule.from_dict({
			'lhs': antecedent,
			'rhs': consequent,
			'sup': support,
			'con': confidence
		})
		self.assertEqual(type(rule), Rule)

		# check counts
		self.assertEqual(rule.antecedent_count(), len(antecedent))
		self.assertEqual(rule.consequent_count(), len(consequent))

		# check that all items were stored
		for lhs_item in antecedent:
			item = RuleItem.from_dict(lhs_item)
			self.assertIn(item, rule.antecedent())

		for rhs_item in consequent:
			item = RuleItem.from_dict(rhs_item)
			self.assertIn(item, rule.consequent())

		# check properties
		self.assertEqual(rule.support(), support)
		self.assertEqual(rule.confidence(), float(confidence))
