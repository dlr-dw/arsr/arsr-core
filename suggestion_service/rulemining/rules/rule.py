# Contains classes RuleItem and Rule

import json
import logging


class RuleItem:
	""" Class representing a single item that is part of a rule. """

	FIELD_KEY = 'field'
	VALUE_KEY = 'value'
	FIELD_TYPE_KEY = 'field_type'

	def __init__(self, field, values, field_type=None):
		"""
		Parameters
		----------
		field : str
			Name of the field.
		values : typing.Iterable | str
			A set of values for this field or a single value as a string.
			It will be stored as a tuple.
		field_type : str | None
			Optional field type (URI of concept).
		"""
		self._field = field 
		if isinstance(values, str): self._values = tuple([values])
		else: self._values = tuple(values)
		self._field_type = field_type

	@property
	def field(self):
		return self._field

	@property
	def values(self):
		return self._values

	@property
	def field_type(self):
		return self._field_type

	def __len__(self):
		""" Returns the amount of values as size of this item. """
		return len(self._values)

	def __str__(self):
		ft_str = f' ({self._field_type})' if self._field_type is not None else ''
		return json.dumps({self._field + ft_str: self._values})

	def __repr__(self):
		return str(self)

	def __eq__(self, other):
		if not isinstance(other, type(self)): return False
		if len(self._values) != len(other.values): return False
		for v in self._values:  # order independent value comparison
			if v not in other.values:
				return False
		same_field_name = self.field == other.field
		same_field_type = self._field_type == other.field_type
		return same_field_name and same_field_type

	def to_dict(self):
		""" Return this rule represented by a dict. """
		d = dict()
		d[RuleItem.FIELD_KEY] = self._field
		if self._field_type is not None:
			d[RuleItem.FIELD_TYPE_KEY] = self._field_type
		d[RuleItem.VALUE_KEY] = self._values
		return d

	@classmethod
	def from_dict(cls, d: dict):
		""" Create instance of a RuleItem from dict.

		Expected format:
		{
			"field": "fieldname",
			"value": ["value1", "value2", ...]
		}
		or
		{
			"field": "fieldname",
			"value": "value1",
			"field_type": "concept_URI"
		}
		"""
		mandatory = (RuleItem.FIELD_KEY, RuleItem.VALUE_KEY)
		missing = []
		for m in mandatory:
			if m not in d:
				missing.append(m)
		if len(missing) > 0:
			raise ValueError(f'Missing {", ".join(missing)}')
		field_type = d.get(RuleItem.FIELD_TYPE_KEY, None)
		return cls(d[RuleItem.FIELD_KEY], d[RuleItem.VALUE_KEY], field_type)


class Rule:
	""" Class to represent a single association rule. """

	LOGGER = logging.getLogger(__name__)

	def __init__(self, X, Y, supp: float, conf: float, check_validity=True):
		"""
		Parameters
		----------
		X : tuple | list
			A list of RuleItem instances representing the rules antecedent.
		Y : tuple | list
			A list of RuleItem instances representing the rule consequent.
		supp : float
			Support value of the rule.
		conf : float
			Rule confidence.
		check_validity : bool
			If True, ensures that each field only occurs once on each side
			of the rule and raises an Exception if this holds not true.
		"""
		self.X = X
		self.Y = Y
		self.supp = supp
		self.conf = conf

		if len(X) == 0:
			raise ValueError('Rule needs at least one item on lhs.')
		if len(Y) == 0:
			raise ValueError('Rule needs at least one item on rhs.')

		self.X_fields = set(item.field for item in self.X)
		self.Y_fields = set(item.field for item in self.Y)

		if check_validity:
			if len(self.X_fields) != len(self.X):
				raise ValueError('A field on the lhs occurs more than once')
			if len(self.Y_fields) != len(self.Y):
				raise ValueError('A field on the rhs occurs more than once')

	def antecedent(self): return self.X
	def consequent(self): return self.Y
	def support(self): return self.supp
	def confidence(self): return self.conf

	def antecedent_count(self):
		""" Returns amount of items in rule antecedent. """
		return len(self.X)

	def consequent_count(self):
		""" Returns amount of items in rule consequent. """
		return len(self.Y)

	def antecedent_fields(self):
		""" Returns field names of items in antecedent as iterator. """
		return iter(self.X_fields)

	def consequent_fields(self):
		""" Returns field names of items in consequent as iterator. """
		return iter(self.Y_fields)

	def __str__(self):
		""" Representation of rule when printing it. """
		lhs = tuple(str(item) for item in self.X)
		rhs = tuple(str(item) for item in self.Y)
		return f'{lhs} -> {rhs} (s: {self.supp}, c: {self.conf})'

	def to_dict(self):
		""" Converts the rule into a dictionary and returns it. """
		return {
			'lhs': [item.to_dict() for item in self.X],
			'rhs': [item.to_dict() for item in self.Y],
			'sup': self.supp,
			'con': self.conf
		}

	@classmethod
	def from_dict(cls, d):
		""" Create Rule instance based on given dict.

		The information of the dict should look like this:
		(see also: structure returned by to_dict())
		{
			"lhs": [
				{
					"field": "field name 1",
					"value": ["value1", "value2", ...]
				},
				{
					"field": "field name 2,
					"value": "value_2",
					"label (optional)": "Value 2"
				},
				...
			],
			"rhs": [
				... same as lhs ...
			],
			"sup": 0.234,
			"con": 0.432
		}

		Parameters
		----------
		d : dict
			Dictionary that holds rule data.
		"""
		mandatory = ('lhs', 'rhs', 'sup', 'con')
		missing = []
		for m in mandatory:
			if m not in d:
				missing.append(m)
		if len(missing) > 0:
			raise ValueError(f'Missing {", ".join(missing)}')

		lhs = []
		for item_dict in d['lhs']:
			lhs.append(RuleItem.from_dict(item_dict))

		rhs = []
		for item_dict in d['rhs']:
			rhs.append(RuleItem.from_dict(item_dict))

		support = float(d['sup'])
		confidence = float(d['con'])
		return cls(lhs, rhs, support, confidence)

	@classmethod
	def decode_itemset(cls, encoded_itemset):
		""" Decodes an itemset that consists of dicts.

		The encoded itemsets are data retrieved from the TransactionConverter.
		This is done at the end of RuleMiningService.generate_rules()
		before storing rules in the rule repository.

		Encoded items look like this: {"field": "fieldname", "value": "actual_value"}
		and there are several of such given by the generator. Note that a fieldname can
		occur multiple times and therefore, is treated accordingly to gather its values.
		The field type should be the same for every same fieldname encountered
		and usually, only occur uniquely for one field.

		Parameters
		----------
		encoded_itemset : typing.Iterable[dict]

		Returns
		-------
		tuple[RuleItem]
			An itemset as a tuple of RuleItem instances.
		"""
		d = dict()

		for item in encoded_itemset:
			field = item['field']
			value = item['value']
			field_type = item.get('field_type')
			if field not in d:
				d[field] = {
					'v': [value],
					't': field_type
				}
			else:
				d[field]['v'].append(value)
				if field_type != d[field].get('t'):  # each field should only have one
					cls.LOGGER.warning(f'Multiple field types for field {field}.')

		# create itemset of RuleItem instances
		item_gen = (RuleItem(f, e['v'], field_type=e.get('t')) for f, e in d.items())
		return tuple(item_gen)
