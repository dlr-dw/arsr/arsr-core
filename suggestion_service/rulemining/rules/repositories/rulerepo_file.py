import json
import logging
import os.path

from suggestion_service.io import IOUtils
from . import RuleRepo, BasicRuleFilter
from ..rule import Rule


class FileRuleRepo(RuleRepo):
	""" Simulates a rule repository by loading data from a file. """

	def __init__(self, filepath, load_now=True, save_on_close=True):
		super().__init__()
		self.logger = logging.getLogger(__name__)
		self.rules = []
		self.filepath = filepath
		self.save_on_close = save_on_close
		if load_now: self._load(filepath)

	def add_rule(self, rule):
		""" Add an instance of a Rule. """
		self.rules.append(rule)

	def add_rules(self, rules):
		self.rules.extend(rules)

	def get_rules(self):
		""" Get a generator to iterate over rules. """
		for rule in self.rules: yield rule

	def get_rules_filtered(self, filterinfo):
		""" Get a generator to iterate over filtered rules. """
		basicfilter = BasicRuleFilter()
		for rule in self.rules:
			if basicfilter.discard(rule, filterinfo): continue
			yield rule

	def is_empty(self):
		return len(self.rules) == 0

	def close(self):
		if self.save_on_close:
			self._write(self.filepath, indent=2)

	def _load(self, filepath: str):
		""" Load rules from JSON (or JSONL) file given by path. """
		self.logger.info(f'Loading rules from file: {filepath}')
		with open(filepath, 'r') as fin:
			ext = os.path.splitext(filepath)[1].lower()  # get extension
			if ext == '.json': loaded = self._load_file(fin)
			elif ext == '.jsonl': loaded = self._load_file_jsonl(fin)
			else: self.logger.error(f'Invalid file extension: {ext}')
		self.logger.info(f'Rules loaded: {loaded}')
		return loaded

	def _load_file(self, file_handle):
		""" Load rules from JSON file handle.

		The file format must look as follows:\n
		{
			"rules": [
				Rule,\n
				...
			]
		}

		See Rule.to_dict() for further information.

		Raises
		------
		json.decoder.JSONDecodeError
			If the JSON to load is invalid.
		ValueError
			If no rules were found in the file.

		Returns
		-------
		int
			Number of rules loaded.
		"""
		if isinstance(file_handle, str):
			raise ValueError('Invalid handle.')

		jdata = json.load(file_handle)
		rules = jdata.get('rules')
		if rules is None:
			raise ValueError(f'No rules found in file: {file_handle.name}')

		# clear currently stored data and load new
		self.rules.clear()
		loaded = 0
		for rule_dict in rules:
			try:
				rule = Rule.from_dict(rule_dict)
				self.rules.append(rule)
				loaded += 1
			except Exception as ex:
				self.logger.exception('Failed to load rule.', ex)
		return loaded

	def _load_file_jsonl(self, file_handle):
		""" Load rules from JSON Lines (.jsonl) file. """
		if isinstance(file_handle, str):
			raise ValueError('Invalid handle.')

		self.rules.clear()
		loaded = 0
		line_num = 0
		for json_str in file_handle.readlines():
			try:
				line_num += 1
				rule_dict = json.loads(json_str)
				rule = Rule.from_dict(rule_dict)
				self.rules.append(rule)
				loaded += 1
			except ValueError as ex:
				self.logger.warning(f'Skipping rule (line {line_num}): {ex}')
			except Exception as ex:
				self.logger.exception(f'Failed to load rule (line {line_num})', ex)
		return loaded

	def _write(self, filepath: str, **kwargs):
		""" Write stored rules to file as JSON.

		Parameters
		----------
		filepath : str
			Name of file or path to file including ".json" extension.
		"""
		if not (filepath.endswith('.json') or filepath.endswith('.jsonl')):
			self.logger.error('Writing to file failed: invalid file extension.')
			return
		path = IOUtils.prepare_file_write(filepath)
		if path is None: raise ValueError(f'Invalid file: {filepath}')
		self.logger.info(f'Writing rules to: {path.resolve()}')
		with path.open('w') as fout:
			ext = os.path.splitext(filepath)[1].lower()  # get extension
			if ext == '.json': self._write_file(fout, **kwargs)
			elif ext == '.jsonl': self._write_file_jsonl(fout)
			else: self.logger.error(f'Writing to file failed: invalid file extension: {ext}')

	def _write_file(self, file_handle, **kwargs):
		""" Write the currently stored rules to the file.

		Parameters
		----------
		file_handle
			File handle used to write to (e.g. created with open()).
		**kwargs
			See available parameters of json.dump() method:
			https://docs.python.org/3/library/json.html#json.dump
		"""
		if isinstance(file_handle, str):
			raise ValueError('Invalid handle.')

		jdata = {
			'rules': list(r.to_dict() for r in self.get_rules())
		}
		json.dump(jdata, file_handle, **kwargs)

	def _write_file_jsonl(self, file_handle):
		""" Write the currently stored rules to the JSON Lines file. """
		if isinstance(file_handle, str):
			raise ValueError('Invalid handle.')

		for rule in self.get_rules():
			rule_dict = rule.to_dict()
			file_handle.write(json.dumps(rule_dict, indent=None))
			file_handle.write('\n')
