import unittest

from ..rulefilter import BasicRuleFilter, RuleFilterInfo
from ...rule import RuleItem, Rule


class TestBasicRuleFilter(unittest.TestCase):
	""" Contains tests for the BasicRuleFilter class. """

	def setUp(self):
		""" Prepare test data. """
		antecedent = (RuleItem('f1', 'v1'), RuleItem('f2', 'v2'))

		# single item in consequent
		self.r1 = Rule(
			antecedent,
			(RuleItem('f3', 'v3'),), # consequent
			0.5, 0.8 # supp and conf
		)

		# multiple items for a single field in consequent
		self.r2 = Rule(
			antecedent,
			(RuleItem('f3', ['v31', 'v32']),),
			0.5, 0.8
		)

		# multiple items for different fields in consequent
		self.r3 = Rule(
			antecedent,
			(RuleItem('f3', 'v3'), RuleItem('f4', 'v4')),
			0.5, 0.8
		)

		# multiple items for different fields and same field in consequent
		self.r4 = Rule(
			antecedent,
			(RuleItem('f3', ['v31', 'v32']), RuleItem('f4', 'v4')),
			0.5, 0.8
		)


	def test_default_filter(self):
		""" Rule filter allowing multiple items in addition to the target. """
		rfilter = BasicRuleFilter()
		info = RuleFilterInfo()

		info.target_field = 'f3'
		self.assertFalse(rfilter.discard(self.r1, info), 'Should not discard')
		self.assertFalse(rfilter.discard(self.r2, info), 'Should not discard')
		self.assertFalse(rfilter.discard(self.r3, info), 'Should not discard')
		self.assertFalse(rfilter.discard(self.r4, info), 'Should not discard')

		info.target_field = 'f4'
		self.assertTrue(rfilter.discard(self.r1, info), 'Should be discarded')
		self.assertTrue(rfilter.discard(self.r2, info), 'Should be discarded')
		self.assertFalse(rfilter.discard(self.r3, info), 'Should not discard')
		self.assertFalse(rfilter.discard(self.r4, info), 'Should not discard')


	def test_single_consequent(self):
		""" Only a single item that is the target field is allowed. """
		rfilter = BasicRuleFilter()
		info = RuleFilterInfo()
		info.target_field = 'f3'
		info.consequent_items = 'single'
		self.assertFalse(rfilter.discard(self.r1, info), 'Should not discard')
		self.assertTrue(rfilter.discard(self.r2, info), 'Should be discarded')


	def test_field_consequent(self):
		""" Multiple items for the target field are allowed in consequent. """
		rfilter = BasicRuleFilter()
		info = RuleFilterInfo()
		info.target_field = 'f3'
		info.consequent_items = 'field'
		self.assertFalse(rfilter.discard(self.r2, info), 'Should not discard')
		self.assertTrue(rfilter.discard(self.r3, info), 'Should be discarded')
		self.assertTrue(rfilter.discard(self.r4, info), 'Should be discarded')
