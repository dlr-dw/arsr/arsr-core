import json
import unittest
from io import StringIO

from .. import FileRuleRepo
from ... import Rule, RuleItem


class TestFileRuleRepo(unittest.TestCase):
	""" Tests some methods of rule repository.
	Uses a strategy to avoid writing to disk as suggested here:
	https://stackoverflow.com/a/3945057
	"""

	def setUp(self):
		# prepare input dictionary
		self.example_rules = [
			Rule(
				[RuleItem('A', 'a1'), RuleItem('B', 'b1')],
				[RuleItem('C', 'c1')],
				0.5, 0.2
			),
			Rule(
				[RuleItem('A', 'a2'), RuleItem('B', 'b1')],
				[RuleItem('C', 'c2')],
				0.3, 0.5
			)
		]

	def test_json_io(self):
		""" Test basic I/O operations of FileFormRepo for JSON files. """
		repo = FileRuleRepo(None, load_now=False, save_on_close=False)
		rules_dict = {
			'rules': [
				self.example_rules[0].to_dict()
			]
		}

		# load from simulated file
		file_in = StringIO(json.dumps(rules_dict))
		loaded = repo._load_file(file_in)
		self.assertEqual(loaded, 1, 'Expecting one rule to be loaded.')

		# add another rule
		repo.add_rule(self.example_rules[1])
		rules_dict['rules'].append(self.example_rules[1].to_dict())

		# now write to a simulated file
		file_out = StringIO()
		repo._write_file(file_out)

		# compare the input JSON string to the output JSON string
		result_str = file_out.getvalue()
		expected_str = json.dumps(rules_dict)
		self.assertEqual(result_str, expected_str)

	def test_jsonl_io(self):
		""" Test basic I/O operations of FileFormRepo for JSON Lines files. """
		repo = FileRuleRepo(None, load_now=False, save_on_close=False)

		# prepare simulated file (add rules)
		rule1_str = json.dumps(self.example_rules[0].to_dict(), indent=None) + '\n'
		rule2_str = json.dumps(self.example_rules[1].to_dict(), indent=None) + '\n'
		rules_str = rule1_str + rule2_str
		file_in = StringIO(rules_str, newline='\n')

		# load from simulated file
		loaded = repo._load_file_jsonl(file_in)
		self.assertEqual(loaded, 2, 'Expecting two rules to be loaded.')

		# now write to a simulated file
		file_out = StringIO()
		repo._write_file_jsonl(file_out)

		# compare the input JSON string to the output JSON string
		result_str = file_out.getvalue()
		self.assertEqual(result_str, rules_str)
