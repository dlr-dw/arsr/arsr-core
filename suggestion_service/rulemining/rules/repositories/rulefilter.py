from enum import Enum
import typing


class RuleFilterInfo:
	""" Provides data and settings for rule filtering done by RProviders. """

	class ConsequentItemsEnum(Enum):
		MANY = 'many'      # allows multiple items in consequent
		SINGLE = 'single'  # only a single item with a single value
		FIELD = 'field'    # only items of a single field

	def __init__(self):
		self._data = {}
		self._consequent_items = RuleFilterInfo.ConsequentItemsEnum.MANY

	@property
	def target_field(self):
		return self._data.get('target_field')

	@target_field.setter
	def target_field(self, value):
		self._data['target_field'] = value

	@property
	def consequent_items(self):
		return self._consequent_items

	@consequent_items.setter
	def consequent_items(self, value: typing.Union[ConsequentItemsEnum, str]):
		""" Configure allowed consequent items. """
		if not isinstance(value, RuleFilterInfo.ConsequentItemsEnum):
			# try to make the value a valid enum
			try:
				value = str(value).lower()
				value = RuleFilterInfo.ConsequentItemsEnum(value)
			except ValueError as ve:
				raise ValueError(str(ve))
		self._consequent_items = value

	def set_data(self, name, value):
		""" Used to provide additional data required for filtering. """
		if self._data is None: self._data = {}
		self._data[name] = value

	def get_data(self, name, default=None):
		""" Retrieve value for given data name or default. """
		if self._data is None: return default
		return self._data.get(name, default)


class BasicRuleFilter:
	""" Basic in-memory filtering functionality with discard(rule). """

	def discard(self, rule, info):
		""" Default in-memory discard filter functionality.

		Parameters
		----------
		rule : Rule
			The rule to check if it should be discarded.
		info : RuleFilterInfo
			Information and settings to control filtering.

		Returns
		-------
		bool
			True if the rule should be discarded and False otherwise.
		"""

		# MANDATORY
		# target_field must be in consequent of rule
		if info.target_field is None: raise ValueError('Missing target_field!')
		if info.target_field not in rule.consequent_fields(): return True

		# OPTIONAL
		# rules with more items are generally allowed and don't break anything
		if info.consequent_items == RuleFilterInfo.ConsequentItemsEnum.SINGLE:
			if rule.consequent_count() > 1: return True
			if len(rule.consequent()[0]) > 1: return True
		elif info.consequent_items == RuleFilterInfo.ConsequentItemsEnum.FIELD:
			if rule.consequent_count() > 1: return True

		# TODO: extend default filter here if required
		return False
