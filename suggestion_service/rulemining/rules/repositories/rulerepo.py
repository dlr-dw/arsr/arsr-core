import typing

from . import RuleFilterInfo
from ..rule import Rule


class RuleRepo:
	""" Base class for rule repository connections. """

	def __init__(self):
		pass

	def add_rule(self, rule: Rule):
		pass

	def add_rules(self, rules: typing.Iterable[Rule]):
		pass

	def get_rules(self):
		""" Get a generator to iterate over rules. """
		pass

	def get_rules_filtered(self, filterinfo : RuleFilterInfo):
		""" Get generator of filtered rules based on settings of filterinfo.

		In case of a database repository, indexing support should be used
		if available and filtering defined accordingly.

		Parameters
		----------
		filterinfo : RuleFilterInfo
			Instance with settings and filter data.

		Returns
		-------
		typing.Iterable[Rule]
		"""
		pass

	def is_empty(self):
		""" Returns True if the repository has no rules. """
		pass

	def close(self):
		""" Close connections and free resources if there are any. """
		pass
