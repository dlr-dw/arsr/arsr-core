from . import RuleRepo, BasicRuleFilter
from ..rule import RuleItem, Rule


class InMemoryRuleRepo(RuleRepo):
	""" Simulates a rule repository by using in-memory data. """

	def __init__(self, **kwargs):
		super().__init__()
		self.rules = []
		if kwargs.get('example') == 'animals':
			self.add_rules(self._animal_rules())

	def add_rule(self, rule):
		""" Add an instance of a Rule. """
		self.rules.append(rule)

	def add_rules(self, rules):
		self.rules.extend(rules)

	def get_rules(self):
		""" Get a generator to iterate over rules. """
		for rule in self.rules: yield rule

	def get_rules_filtered(self, filterinfo):
		""" Get a generator to iterate over filtered rules. """
		basicfilter = BasicRuleFilter()
		for rule in self.rules:
			if basicfilter.discard(rule, filterinfo): continue
			yield rule

	def is_empty(self):
		return len(self.rules) == 0

	@classmethod
	def example_animals(cls):
		""" Creates instance of a InMemoryRuleRepo and add animal examples. """
		return cls(example='animals')

	@staticmethod
	def _animal_rules():
		""" Returns a tuple of 10 instances for animal example. """
		# Generated from in-memory animal example using apriori with s=0.3 and c=0.6
		def item(field, value):
			return RuleItem(field, value)
		return (
			Rule([item('tags', 'whiskers')], [item('fly', 'no')], 0.3, 1.0),
			Rule([item('type', 'mammal')], [item('fly', 'no')], 0.4, 0.8),
			Rule([item('tags', 'wings')], [item('fly', 'yes')], 0.3, 0.75),
			Rule([item('fly', 'yes')], [item('tags', 'wings')], 0.3, 1.0),
			Rule([item('type', 'mammal')], [item('tags', 'claws')], 0.3, 0.6),
			Rule([item('tags', 'claws')], [item('type', 'mammal')], 0.3, 1.0),
			Rule([item('type', 'mammal')], [item('tags', 'whiskers')], 0.3, 0.6),
			Rule([item('tags', 'whiskers')], [item('type', 'mammal')], 0.3, 1.0),
			Rule([item('type', 'bird')], [item('tags', 'wings')], 0.3, 1.0),
			Rule([item('tags', 'wings')], [item('type', 'bird')], 0.3, 0.75),
			Rule([item('type', 'mammal')], [item('fly', 'no'), item('tags', 'whiskers')], 0.3, 0.6),
			Rule([item('tags', 'whiskers')], [item('fly', 'no'), item('type', 'mammal')], 0.3, 1.0),
			Rule([item('tags', 'whiskers'), item('type', 'mammal')], [item('fly', 'no')], 0.3, 1.0),
			Rule([item('fly', 'no'), item('type', 'mammal')], [item('tags', 'whiskers')], 0.3, 0.75),
			Rule([item('fly', 'no'), item('tags', 'whiskers')], [item('type', 'mammal')], 0.3, 1.0),
		)
