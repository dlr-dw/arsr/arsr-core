# Run this example using: python -m rulemining.example

from . import RuleMiningService
from .forms import repositories as formrepos
from .methods import MethodFactory
from .rules import repositories as rulerepos


def main():
	""" Example application for using rule mining service. """
	
	# association rule mining service
	arms = RuleMiningService(verbosity=1)

	# get in-memory repository with examples
	formrepo = formrepos.InMemoryFormRepo.example_animals()
	print('Loaded animal example instances.')

	# get in-memory rule repository for storing rules
	rulerepo = rulerepos.InMemoryRuleRepo()

	# create instance of a rule mining method
	apriori = MethodFactory.create('apriori', minsupp=0.3, minconf=0.7)

	# perform actual association rule mining
	arms.generate_rules(formrepo, apriori, rulerepo)

	# retrieve rules from rule repository
	print('\nRules:')
	for rule in rulerepo.get_rules(): print(rule)


if __name__ == '__main__':
	main()
