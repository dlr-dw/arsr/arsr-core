import json
import typing

from suggestion_service.rulemining.rules.repositories import RuleRepoFactory
from .rproviders import RProviderFactory
from .pipeline import PipelineConfig, Pipeline


class SuggestionServiceConfig:
	""" Configuration for suggestion service execution. """

	def __init__(self, path):
		self._rprovider = None
		self._pipeline_config = {}
		self._pipeline = None
		self._rulerepo = None
		self._populated = {}
		self._target_field = ''
		self._gather_stats = False
		self._verbosity = 0
		self._load_file(path)

	@property
	def rprovider(self):
		return self._rprovider

	@property
	def pipeline_config(self) -> typing.Union[PipelineConfig, dict]:
		return self._pipeline_config

	@property
	def pipeline(self):
		return self._pipeline

	@property
	def verbosity(self):
		return self._verbosity

	@property
	def rulerepo(self):
		return self._rulerepo

	@property
	def populated(self):
		return self._populated

	@property
	def target_field(self):
		return self._target_field

	@property
	def gather_stats(self):
		return self._gather_stats

	def _load_file(self, path):
		with open(path, 'r') as fin:
			self._load_config(fin)

	def _load_config(self, file_handle):
		""" Loads config from file contents.
		{
			"name": "Config Name",
			"description (optional)": "config description",
			"config": {
				"populated": {
					"field1": "value1",
					"field2": ["value1", "value2", ...],
					...
				},
				"target_field": "target field name",
				"verbosity (optional)": 0,
				"gather_stats (optional)": false,
				"rulerepo": {
					"name": "type of repo (see RuleRepoFactory)",
					"config": { ... }
				},
				"relatedness_provider": {
					"name": "type of provider (see RProviderFactory)",
					"config": { ... }
				},
				"pipeline (optional arsr pipeline config)": {
					... (see Pipeline config schema) ...
				},
				"gather_stats (optional for evaluation)": false
			}
		}
		"""
		jdata = json.load(file_handle)
		# TODO: schema validation

		confname = jdata.get('name')
		if confname is not None:
			print(f'Loading suggestion service config: {confname}')

		# Set properties of this configuration
		c_main = jdata.get('config')

		# populated and target_field can be optional (e.g. when using from API)
		self._populated = c_main.get('populated', self._populated)
		self._target_field = c_main.get('target_field', self._target_field)

		self._verbosity = c_main.get('verbosity', self._verbosity)
		gather_stats = c_main.get('gather_stats', None)

		# Configure rule repository
		c_rulerepo = c_main.get('rulerepo')
		self._rulerepo = RuleRepoFactory.create(
			c_rulerepo.get('name'),
			**c_rulerepo.get('config', {})
		)

		# Configure relatedness provider
		c_rprovider = c_main.get('relatedness_provider')
		rprovider_conf = c_rprovider.get('config', {})
		if gather_stats is not None:
			rprovider_conf['gather_stats'] = gather_stats
		self._rprovider = RProviderFactory.create(
			c_rprovider.get('name'),
			**rprovider_conf
		)

		# Configure pipeline
		pipeline_conf = c_main.get('pipeline')
		if gather_stats is not None:
			pipeline_conf['gather_stats'] = gather_stats
		self._pipeline_config = PipelineConfig(pipeline_conf)
		self._pipeline_config.validate()
		self._pipeline = Pipeline(
			self._rprovider,
			self._pipeline_config,
			verbosity=self._verbosity)

		# Other
		if gather_stats is not None:
			self._gather_stats = gather_stats
