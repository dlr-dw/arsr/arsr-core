import logging
import time
from . import utils
from .pipeline_base import APipeline
from .. import scoring
from .pipeline_config import PipelineConfig
from ..scoring import SemanticScorer, RuleScorer, ScoreAggregator


class Pipeline(APipeline):
	""" Scoring pipeline to create suggestions from rules. """

	def __init__(self, relatedness_provider, config: PipelineConfig, verbosity=0):
		""" Create a new scoring pipeline.

		This will set up the semantic scorer, rule scorer and score aggregator
		components for the given configuration.

		Parameters
		----------
		relatedness_provider : RProvider
			Instance of a relatedness provider, used by the SemanticScorer.
		config : PipelineConfig
			Configure the pipeline based on this configuration.
		verbosity : int
			Enable debug printouts by setting a value greater than zero.
		"""
		self.logger = logging.getLogger(__name__)

		# get aggregation methods to use or None (e.g. in case of evaluation)
		fam = config.field_aggregation_method()
		fam_name = fam
		if fam is not None:  # try to find it
			fam = scoring.AggregationMethods.get_method(fam)
			if fam is None: raise ValueError('Invalid field aggregation method!')

		sam = config.score_aggregation_method()
		sam_name = sam
		if sam is not None:  # try to find it
			sam = scoring.AggregationMethods.get_method(sam)
			if sam is None: raise ValueError('Invalid score aggregation method!')

		self.fam_sam = (fam_name, sam_name)
		self._evaluation = config.evaluation()
		if self._evaluation is False and (fam is None or sam is None):
			raise ValueError('Missing aggregation method(s)!')
		# self.sort_results = config.sort_results()  # only for non-eval mode

		self.rprovider = relatedness_provider
		self.sscorer = SemanticScorer(relatedness_provider, verbosity)
		self.rscorer = RuleScorer(fam, self._evaluation)
		self.saggreg = ScoreAggregator(sam, self._evaluation, config.score_precision())

		# configure rule scorer
		self.rscorer.skip_zero_score(config.skip_zero_score())
		self.rscorer.apply_jaccard_index(config.apply_jaccard_index())
		self.rscorer.apply_confidence(config.apply_confidence())
		self.rscorer.apply_consequent_fit(config.apply_consequent_fit())

		self.jac_t = 0  # 0 means "allow all" (disable this rule filter)
		self.verbosity = verbosity if verbosity is not None else 0
		self.gather_stats = False
		self.stats = None  # stats are mainly used for evaluation
		self.discard_already_populated = True

		self.configure(
			config.skip_no_pairs(),
			config.skip_zero_score(),
			config.jaccard_threshold(),
			config.gather_stats(),
			config.discard_already_populated()
		)

	def configure(self, skip_no_pairs=True, skip_zero_score=True,
		jaccard_threshold=0, gather_stats=False, discard_already_populated=True):
		""" Configure this instance.

		Parameters
		----------
		skip_no_pairs : bool
			Skip rules for which no entity-pairs could be extracted.
			No pairs means none of the populated fields is contained.
		skip_zero_score : bool
			If False, add rules with a final score of zero to the results,
			otherwise discard them. In case of rules with multiple final scores
			(i.e. evaluation mode) all final scores must be zero to discard it.
		jaccard_threshold : float
			Skip processing rules with jaccard index below this threshold.
			The jaccard index states how well the rule antecedent
			matches the populated form wrt. the fields.
			Set value <= 0 to disable this feature.
		discard_already_populated : bool
			Discard suggestions already in the populated form field.
		"""
		self.sscorer.skip_no_pairs(skip_no_pairs)
		self.rscorer.skip_zero_score(skip_zero_score)
		self.jac_t = jaccard_threshold
		self.gather_stats = gather_stats
		self.discard_already_populated = discard_already_populated

	def log_config(self):
		""" Printout current configuration to logger. """
		fam_sam_str = ''
		if not self._evaluation:
			fam_sam_str = f', FieldAM: {self.fam_sam[0]}, ScoreAM: {self.fam_sam[1]}'
		self.logger.info(
			f'Pipeline settings: jaccard_threshold: {self.jac_t}, evaluation: {self._evaluation}{fam_sam_str}, '
			f'discard_already_populated: {self.discard_already_populated}; '
			f'Rule scorer: {self.rscorer.get_config_str()}; '
			f'Semantic Scorer: {self.sscorer.get_config_str()}'
		)

	@property
	def evaluation(self):
		return self._evaluation

	def score(self, filtered_rules, populated_fields, target_field):
		""" Runs pipeline to create scored suggestions.

		Parameters
		----------
		filtered_rules : typing.Iterable[Rule]
			An iterable of already filtered Rule objects.
		populated_fields : dict
			Currently populated fields and their values, e.g.:
			{
				"f1_label": "f1_value",
				"f2_label": ["f2_v1", "f2_v2", ...],
				...
			}
		target_field : str
			Name of the target field for which suggestions are generated.

		Returns
		-------
		dict
			Dictionary of suggestions. In case of evaluation mode, each key is
			a suggestion and contains combinations of used aggregation methods
			and score. Otherwise, key is a suggestion and value its score.
		"""
		gsinfo = ' (gathering stats)' if self.gather_stats is True else ''
		self.logger.info(f'Pipeline scoring started{gsinfo}...')

		# validate and clean up populated fields
		populated_fields = utils.prepare_populated_fields(populated_fields)
		if len(populated_fields) == 0:
			self.logger.info('No populated fields.')
			return {}

		# for evaluation stats
		cnt_skipped_jaccard = 0
		cnt_skipped_semanticscorer = 0
		cnt_skipped_rulescorer = 0

		# stores suggestions and their score(s)
		suggestions = dict()

		rno = 0
		t0 = time.perf_counter()

		for rule in filtered_rules:

			rno += 1
			if self.verbosity > 0: print(f'\nProcessing rule {rno}: {rule}')
			scored_rule = scoring.ScoredRule.from_rule(rule)

			# extract suggestions from rule rhs for target field and add to set
			# (must be done anywhere before finally gathering scored suggestions)
			scored_rule.add_suggestions(self.__extract_suggestions(rule, target_field))

			# compute additional scores of rule for later use (e.g. jaccard)
			# (must be done before Rule Scorer scoring as it uses them)
			scored_rule.add_scores(self.__rule_scores(rule, populated_fields, target_field))

			# discard rules with jaccard index below certain threshold
			if scored_rule.get_score('jaccard_index', 1) < self.jac_t:
				if self.gather_stats: cnt_skipped_jaccard += 1
				continue

			# A) SEMANTIC SCORER (add semantic scores to fields)
			discard = self.sscorer.score(rule, populated_fields, scored_rule)
			if discard:  # e.g. if no entity pairs
				if self.gather_stats: cnt_skipped_semanticscorer += 1
				continue

			# B) RULE SCORER (aggregate field scores for suggestion scores)
			# Creates what CVR called the "recommendation_score(v)".
			fieldscores = list(scored_rule.get_field_scores(scores_only=True))
			discard = self.rscorer.score(scored_rule, fieldscores)
			if discard:  # e.g. if final score is zero
				if self.gather_stats: cnt_skipped_rulescorer += 1
				continue

			# gather suggestions and their scores in the "suggestions" dict
			if self.verbosity > 0: print(scored_rule)
			self.__gather_suggestions(scored_rule, suggestions)

		# discard suggestions already in the populated form
		if self.discard_already_populated and target_field in populated_fields:
			pv = populated_fields[target_field]
			pop_values = pv if not isinstance(pv, str) else [pv]
			for value in pop_values:
				if value in suggestions:
					suggestions.pop(value)

		# C) SCORE AGGREGATOR (final score per suggestion)
		if self.verbosity > 0: print(f'\nSuggestions: {suggestions}')
		suggestions_scored = self.saggreg.aggregate(suggestions)

		time_diff = round(time.perf_counter() - t0, 4)
		self.logger.info(f'Rules processed: {rno}, Runtime: {time_diff} sec.')

		# gather stats for evaluation
		if self.gather_stats:
			self.stats = {
				'runtime_scoring': time_diff,
				'num_rules_total': rno,
				'skipped_jaccard': cnt_skipped_jaccard,
				'skipped_semanticscorer': cnt_skipped_semanticscorer,
				'skipped_rulescorer': cnt_skipped_rulescorer
			}
			for k, v in self.sscorer.get_stats().items(): self.stats[k] = v
			for k, v in self.rprovider.get_stats().items(): self.stats[k] = v
			self.sscorer.reset_stats()
			self.rprovider.reset_stats()

		# return a dict in both cases (evaluation and non-evaluation)
		# note that dict structure depends on the case
		return suggestions_scored

	@staticmethod
	def __rule_scores(rule, populated_fields, target_field):
		""" Computes additional rule scores.

		1. Jaccard Index of the rule antecedent and populated fields.
		States how many fields both have in common wrt. the union of fields.
		=> jaccard_index

		2. For rules with more than one field in their consequent,
		a factor that states how many items belong to the target field.
		Usable as a penalty factor if there are more fields than required.
		=> consequent_fit

		Returns
		-------
		dict
			A dictionary where key is the score name and value the score, e.g.:
			{
				'jaccard_index': 0.5,
				'consequent_fit': 0.2
			}
		"""
		scores = {}

		# Jaccard Index
		rule_fields = set(rule.antecedent_fields())
		form_fields = populated_fields.keys()
		jaccard = scoring.Utils.jaccard_index(rule_fields, form_fields)
		scores['jaccard_index'] = jaccard

		# Consequent Fit
		cons_fields = set(rule.consequent_fields())
		fitting = sum(len(i) if i.field.casefold() == target_field.casefold() else 0 for i in rule.consequent())
		scores['consequent_fit'] = fitting / len(cons_fields)

		return scores

	@staticmethod
	def __extract_suggestions(rule, target_field):
		""" Extracts all values from rule consequent for target field.
		Returns a generator that yields each suggestion.
		"""
		for item in rule.consequent():
			field = item.field
			value = item.values
			# value is either list of strings or just string
			if field.lower() == target_field.lower():
				if not isinstance(value, str):  # list
					for v in value: yield v
				else:  # string
					yield value

	def __gather_suggestions(self, scored_rule, suggestions_dict):
		""" Add suggestions and score(s) of the rule to the dictionary. """
		for s in scored_rule.get_suggestions():
			if s not in suggestions_dict: suggestions_dict[s] = []
			if self._evaluation:  # add all (aggmethod, score) tuples
				suggestions_dict[s].extend(scored_rule.get_final_scores())
			else:  # add the single score
				suggestions_dict[s].append(scored_rule.get_final_score())

	def get_stats(self):
		""" Get the dict of gathered stats or empty. """
		if self.stats is None: return {}
		return self.stats
