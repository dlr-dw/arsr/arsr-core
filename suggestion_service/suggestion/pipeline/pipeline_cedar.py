import logging
import unicodedata
import time

import regex  # pip install regex
from . import utils
from .pipeline_base import APipeline
from ..scoring import Utils as ScoringUtils


class CedarPipeline(APipeline):
	""" Pipeline for scoring like CEDAR Value Recommender for evaluation.

	Keep in mind that this pipeline does not consider multiple field values.
	In case it encounters such a field, it takes its first value.
	Furthermore, it only exists for evaluation purposes and provides
	the basic idea of scoring presented in the according paper from 2019.
	A detailed Java implementation is provided by the CVR authors here:
	https://github.com/metadatacenter/cedar-valuerecommender-server
	"""

	def __init__(self, discard_zero_score=True, gather_stats=False):
		self.logger = logging.getLogger(__name__)
		self.discard_zero_score = discard_zero_score
		self.gather_stats = gather_stats
		self.stats = None

	@property
	def evaluation(self):
		""" There is no specific evaluation mode for this pipeline. """
		return False

	def get_stats(self):
		""" Get the dict of gathered stats or empty. """
		if self.stats is None: return {}
		return self.stats

	def score(self, filtered_rules, populated_fields, target_field):
		""" Runs pipeline to create scored suggestions.

		Parameters
		----------
		populated_fields : dict
			Currently populated fields, e.g.:
			{
				"f1_label": "f1_value",
				"f2_label": ["f2_v1", "f2_v2", ...],
				...
			}

		Returns
		-------
		dict
			Dictionary of suggestions. Key is a suggestion and value its score.
			Note that Cedar Value Recommender usually sorts values with the
			same score by rule support. We omit this sorting in our evaluation.
		"""
		gsinfo = ' (gathering stats)' if self.gather_stats is True else ''
		self.logger.info(f'Cedar Pipeline scoring started{gsinfo}...')

		# validate and clean up populated fields
		populated_fields = utils.prepare_populated_fields(populated_fields)
		if len(populated_fields) == 0:
			self.logger.info('No populated fields.')
			return {}

		# stores suggestions and their score(s)
		suggestions = dict()

		rno = 0
		t0 = time.perf_counter()

		for rule in filtered_rules:
			rno += 1

			# prepare rule antecedent for scoring
			antecedent = []
			for item in rule.antecedent():
				antecedent.append({
					'field': item.field,
					'value': item.values[0]  # select first value only
				})

			# prepare recommendation context for scoring
			context = []
			for field, values in populated_fields.items():
				# get value, even if it's possibly a list
				if isinstance(values, str):
					value = values
				else:
					try:
						vit = iter(values)
						value = str(next(vit))  # get first item
					except TypeError:
						value = str(values)
				context.append({
					'field': field,
					'value': value
				})

			# compute context matching score
			context_matching_score = ScoringUtils.jaccard_index(
				antecedent, context,
				equal_func=self._item_matches_field
			)

			# compute recommendation score
			recommendation_score = context_matching_score * rule.confidence()

			# Pick the item for the target field and its first value only.
			# In general, CVR expects a single item on the rhs of the rule.
			suggestion = None
			for citem in rule.consequent():
				if citem.field.lower() == target_field.lower():
					suggestion = citem.values[0]
			if suggestion is None: continue

			# Pick the value with the highest score and discard zero
			score_min = 0 if self.discard_zero_score else -1
			if recommendation_score > suggestions.get(suggestion, score_min):
				suggestions[suggestion] = recommendation_score
				# Note: original CVR would also store rule support for final sorting

		time_diff = round(time.perf_counter() - t0, 4)
		self.logger.info(f'Rules processed: {rno}, Runtime: {time_diff} sec.')

		if self.gather_stats:
			self.stats = {
				'runtime_scoring': time_diff,
				'num_rules_total': rno
			}

		return suggestions

	def _item_matches_field(self, item, populated):
		""" Check if rule item matches populated field. """

		# NOTE: we don't check the same way as the Cedar Value Recommender
		# because it is not required for our evaluation.
		item_fieldpath = item['field']
		if not item_fieldpath.startswith('http'):
			item_fieldpath = self._normalize_field_path(item_fieldpath)

		context_fieldpath = populated['field']
		if not context_fieldpath.startswith('http'):
			context_fieldpath = self._normalize_field_path(context_fieldpath)

		# get values to compare (only normalize if its no URI)
		item_value = item['value']
		if not item_value.startswith('http'):
			item_value = self._normalize_field_value(item_value)

		context_value = populated['value']
		if not context_value.startswith('http'):
			context_value = self._normalize_field_value(context_value)

		# check if rule item and populated field match
		return item_fieldpath == context_fieldpath and item_value == context_value

	@staticmethod
	def _normalize_field_path(path):
		""" Normalize field text path (not for URIs).
		As in CVR: valuerecommender/util/CedarTextUtils.normalizePath(path)
		"""
		# See: https://docs.python.org/3/library/stdtypes.html#str.casefold
		path = path.strip().casefold()  # since python 3.3
		return unicodedata.normalize('NFKD', path)

	@staticmethod
	def _normalize_field_value(value):
		""" Normalize a field text value (not for URIs).
		Regex matching requires: https://github.com/mrabarnett/mrab-regex
		Install it using "pip install regex".
		"""
		value = value.strip().casefold()
		value = unicodedata.normalize('NFKD', value)  # we use NFKD instead
		# https://www.regular-expressions.info/unicode.html#prop
		# \p{L} = any kind of letter from any language
		# \p{Nd} = a digit zero through nine
		# Removes (Unicode) chars that are neither letters nor (decimal) digits
		# Example: My•Test•String1234•
		# >>> regex.findall(r'[^\p{L}]+', 'My•Test•String1234•') # no letter
		# ['•', '•', '1234•']
		# >>> regex.findall(r'[^\p{Nd}]+', 'My•Test•String1234•') # no digit
		# ['My•Test•String', '•']
		pattern = r'[^\p{L}\p{Nd}]+'  # NOTE: requires pip regex module to work
		return regex.sub(pattern, '', value)
