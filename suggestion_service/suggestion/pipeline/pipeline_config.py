import json

import jsonschema  # pip install jsonschema

from .pipeline_config_schema import schema


class PipelineConfig(dict):
	""" Class for pipeline configuration that extends a dict. """

	# NOTE: See method _default() for configuration parameters and meaning
	# or have a look at the schema definition in pipeline_config_schema.py
	_schema = schema

	def __init__(self, initconf=None):
		""" Instantiate a pipeline configuration.

		Parameters
		----------
		initconf : dict or PipelineConfig
			An initial configuration to apply (can overwrite defaults).
		"""
		super().__init__()
		self.update(self._default())  # always use default settings as base
		if initconf is not None:
			self.update(initconf)  # add initconf

	def set(self, name, value):
		self[name] = value  # get() is already defined by dict

	def validate(self):
		""" Validate against schema. Raises an exception if invalid. """
		jsonschema.validate(instance=self, schema=PipelineConfig._schema)

	@staticmethod
	def __str_or_none(value):
		""" Returns str of the value or None if its None. """
		if value is None: return value
		vstr = str(value).strip().lower()
		if len(vstr) == 0: return None
		return vstr

	def field_aggregation_method(self):
		return self.__str_or_none(self.get('field_aggregation_method'))

	def score_aggregation_method(self):
		return self.__str_or_none(self.get('score_aggregation_method'))

	def evaluation(self):
		return self.get('evaluation', False)

	def gather_stats(self):
		return self.get('gather_stats', False)

	def skip_no_pairs(self):
		return self.get('skip_no_pairs', True)

	def skip_zero_score(self):
		return self.get('skip_zero_score', True)

	def score_precision(self):
		return self.get('score_precision', 6)

	def jaccard_threshold(self):
		return self.get('jaccard_threshold', 0.5)

	def discard_already_populated(self):
		return self.get('discard_already_populated', True)

	def apply_confidence(self):
		return self.get('final_score', {}).get('apply_confidence', True)

	def apply_jaccard_index(self):
		return self.get('final_score', {}).get('apply_jaccard_index', True)

	def apply_consequent_fit(self):
		return self.get('final_score', {}).get('apply_consequent_fit', False)

	@classmethod
	def _default(cls):
		""" Returns a dictionary with default configuration. """
		# NOTE: If changed, update schema and above methods as well.
		return {
			'evaluation': False,  # if True, agg. methods must not be set
			'gather_stats': False,  # if True, gather stats for evaluation
			'field_aggregation_method': 'median',  # method name or None
			'score_aggregation_method': 'median',  # method name or None
			'skip_no_pairs': True,  # skip rules without entity pairs
			'skip_zero_score': True,  # skip rules with final score of zero
			'score_precision': 6,  # digits for rounding final score or None
			'jaccard_threshold': 0.5,  # skip rules with jaccard index below
			'discard_already_populated': True,  # discard suggestions already in the populated field
			'final_score': {
				'apply_confidence': True,
				'apply_jaccard_index': True,
				'apply_consequent_fit': False
			}
		}

	@classmethod
	def from_json(cls, json_str):
		""" Load config from JSON string (validated against schema). """
		jdata = json.loads(json_str)
		instance = cls(jdata)
		instance.validate()
		return instance
