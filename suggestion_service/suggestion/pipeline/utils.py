# Utils provides some utility functions for the pipelines.
# For instance, to validate and clean up the populated fields input.
# Note: Validation and cleanup of populated fields will probably
# be no longer required once the FormInstance class is used.

import jsonschema  # pip install jsonschema


# used for quick json validation of populated fields
populated_fields_schema = {
	"title": "Populated Fields Schema",
	"type": "object",
	"additionalProperties": {
		"oneOf": [
			{"type": ["string", "number", "integer", "boolean"]},
			{
				"type": "array",
				"items": {
					"type": ["string", "number", "integer", "boolean"]
				},
				# "minItems": 1,  # enforce at least one item
				# "uniqueItems": True  # enforce unique items
			}
		]
	}
}


def validate_populated_fields(populated_fields: dict):
	""" Check that populated fields dict has a valid format. """
	jsonschema.validate(instance=populated_fields, schema=populated_fields_schema)


def cleanup_populated_fields(populated_fields: dict):
	""" Clean up populated fields dict by removing empty entries.
	Also takes care of values occurring multiple times per field to take just one of them.

	Field cleanup examples:
		- [], [""], [" "] or just "", " " as values are discarded
		- ["A", "B", "A"] => ["A", "B"]

	Returns
	-------
	dict
		A copy of the original dictionary if it has been modified.
		Otherwise, just returns the original dictionary.
	"""

	def is_list(val):
		""" Checks if a value is iterable and no string. """
		try:
			iter(val)
		except TypeError:
			return False
		return not isinstance(val, str)

	cleaned = dict()
	for field, value in populated_fields.items():
		if not is_list(value):
			value = str(value).strip()
			if len(value) > 0: cleaned[field] = value
		else:
			# picks just one if value occurs multiple times
			values_new = []
			for v in value:
				if not isinstance(v, str): v = str(v)
				v = v.strip()
				if len(v) > 0 and v not in values_new:
					values_new.append(v)
			if len(values_new) > 0: cleaned[field] = values_new
	return cleaned


def prepare_populated_fields(populated_fields: dict):
	""" Validate and clean up populated fields. Raises exceptions if invalid. """
	if not isinstance(populated_fields, dict):
		raise TypeError(f'populated_fields has invalid type: {type(populated_fields)}')
	try: validate_populated_fields(populated_fields)
	except Exception: raise ValueError(f'populated fields invalid')
	return cleanup_populated_fields(populated_fields)
