class APipeline:
	""" Base class for scoring pipelines. """

	def score(self, filtered_rules, populated_fields, target_field):
		""" Creates suggestions based on populated fields for target field.

		Parameters
		----------
		filtered_rules : typing.Iterable[suggestion_service.rulemining.rules.Rule]
			An iterable of already filtered Rule objects.
		populated_fields : dict
			Currently populated fields, e.g.:
			{
				"f1_label": "f1_value",
				"f2_label": ["f2_v1", "f2_v2", ...],
				...
			}
		target_field : str
			Name of the target field for which suggestions are generated.

		Returns
		-------
		dict
			Dictionary of suggestions. In case of evaluation mode, each key is
			a suggestion and contains combinations of used aggregation methods
			and score. Otherwise, key is a suggestion and value its score.
		"""
		pass

	def get_stats(self):
		""" Optionally gather and return some scoring stats. """
		return {}
