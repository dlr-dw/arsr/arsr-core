from ..scoring import AggregationMethods


# JSON schema used for validation (https://json-schema.org/)
schema = {
	"title": "Pipeline Configuration",
	"description": "Configuration of the scoring pipeline",
	"type": "object",
	"properties": {
		"evaluation": {
			"description": "Enable or disable evaluation mode",
			"type": "boolean",
			"default": "false"
		},
		"gather_stats": {
			"description": "Enable or disable gathering execution stats",
			"type": "boolean",
			"default": "false"
		},
		"field_aggregation_method": {
			"description": "Aggregation method used for field scores",
			"enum": [
				# "median", "mean", "min", "max", "product", "random", null
				*AggregationMethods.get_methods(),
				None
			]
		},
		"score_aggregation_method": {
			"description": "Aggregation method used for rule scores",
			"enum": [
				# "median", "mean", "min", "max", "product", "random", null
				*AggregationMethods.get_methods(),
				None
			]
		},
		"skip_no_pairs": {
			"description": "Discard rules without entity pairs",
			"type": "boolean",
			"default": "true"
		},
		"skip_zero_score": {
			"description": "Discard rules with a final score of zero",
			"type": "boolean",
			"default": "true"
		},
		"score_precision": {
			"description": "Rounding precision of final score",
			"type": ["number", "null"]
		},
		"jaccard_threshold": {
			"description": "Discard rules with jaccard index below",
			"type": "number",
			"default": 0.5
		},
		"discard_already_populated": {
			"description": "Discard suggestions that are already in the populated form field",
			"type": "boolean",
			"default": "true"
		},
		"final_score": {
			"type": "object",
			"properties": {
				"apply_confidence": {
					"type": "boolean",
					"default": "true"
				},
				"apply_jaccard_index": {
					"type": "boolean",
					"default": "true"
				},
				"apply_consequent_fit": {
					"type": "boolean",
					"default": "false"
				}
			}
		}
	},
	"required": []
}
