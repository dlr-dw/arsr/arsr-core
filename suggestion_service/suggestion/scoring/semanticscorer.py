from .aggregation import AggregationMethods
from .entitypair import EntityPair
from ..rproviders import RProvider


class SemanticScorer:
	""" First component to assign a semantic score to rule items. """

	def __init__(self, provider: RProvider, verbosity=0, gather_stats=False):
		"""
		Parameters
		----------
		provider : RProvider
			Instance of a relatedness provider which must
			at least implement methods of the RProvider class.
		gather_stats : bool
			Enable gathering stats. Retrieve them using get_stats().
		"""
		self.provider = provider
		self.verbosity = verbosity

		# default configuration
		self._skip_no_pairs = False

		# store stats for evaluation
		self.stats = None
		self.gather_stats = gather_stats
		self.reset_stats()

	def skip_no_pairs(self, skip: bool):
		""" Skip further processing of rules without entity pairs.
		No pairs means none of the populated fields is contained.
		"""
		if skip is None: return
		self._skip_no_pairs = skip

	def get_config_str(self):
		""" Get current configuration as a string. """
		return f'skip_no_pairs: {self._skip_no_pairs}'

	def score(self, rule, populated_fields, scored_rule):
		""" Score a rule using the assigned relatedness provider.

		Adds field scores to the passed ScoredRule instance.

		Returns
		-------
		bool
			True if the scored rule should be discarded
			(e.g. because no entity pairs could be extracted).
		"""

		# 1. Extract all the entity pairs for relatedness computation
		# - entities = list of entity pairs for each rule field
		# - pairs = set of all extracted entity pairs (references!)
		entities, pairs = self.__extract_entities(rule, populated_fields)

		# No pairs means none of the populated fields is contained.
		# We could still use such rules in the case that all suggestions
		# receive a score of 0 and we need to use rel. freq. as fallback.
		if self.skip_no_pairs and len(pairs) == 0: return True

		# 2. Score entity pairs (relatedness of corresponding field values)
		# - caching is done by the provider, uncached ones are computed
		# - the following call sets relatedness of EntityPair objects
		#   (note that this will also affect the objects in "entities")
		# TODO: handle exceptions (e.g. if LDS connection fails)?
		self.provider.relatedness(pairs)
		if self.verbosity > 0: print(f'Entity Pairs: {entities}')  # DEBUG

		# 3. Compute and assign field scores
		# - for fields with single values, this is just the relatedness
		# - for fields with multiple values, aggregation is required
		cnt_multi_agg = 0
		for field, vpairs in entities.items():

			if len(vpairs) == 1:
				scored_rule.set_field_score(field, vpairs[0].relatedness)
				continue

			# SPECIAL CASE: we previously built any combination of pairs
			# based on that, we now want a single score for the field
			relatedness_gen = (p.relatedness for p in vpairs)
			# per default, use max as discussed in meeting (10.03.22)
			# TODO: used aggregation method should be configurable
			rel_score = AggregationMethods.maximum(relatedness_gen)
			scored_rule.set_field_score(field, rel_score)
			cnt_multi_agg += 1

		if self.gather_stats:
			self.stats['multi_field_agg'] += cnt_multi_agg

		return False

	@staticmethod
	def __extract_entities(rule, populated_fields):
		""" Extract entity pairs from rule and form.

		Note: We want to tell how well a rule "matches" the current form
		and therefore, we say relatedness(rule_value, form_value), i.e. how
		related the rule value is to the form value of a specific field.

		Parameters
		----------
		rule : Rule
			An association rule.
		populated_fields : dict
			A dictionary representing the current form.

		Returns
		-------
		tuple
			A tuple (dict, list) where the dictionary holds a list of EntityPair's for each rule field.
			Following shows them as tuples, but they are actually EntityPair objects.
			{
				"field1": [("v1", "v2")],
				"field2": [("v1", "v2"), ("v1", "v3"), ...],
				...
			}
			The list is a collection of all EntityPair's (references), e.g.:
			{ ("v1", "v2"), ("v1", "v3"), ("v3", "v1"), ... }
		"""

		def fields_equal(f1, f2):
			""" Determine if fields are the same (case insensitive). """
			return f1.lower() == f2.lower()

		def is_list(val):
			""" Checks if a value is a list but no string. """
			try: iter(val)
			except TypeError: return False
			return not isinstance(val, str)

		entities = dict()
		allpairs = list()

		# Usually, rules and forms don't contain that many fields.
		# Therefore, the following approach should be sufficient.
		# TODO: improve comparison if required
		for item in rule.antecedent():
			rfield = item.field  # rule field
			for ffield in populated_fields.keys():  # form field
				if not fields_equal(rfield, ffield): continue

				# get values of both, rule and form field
				rvalue = item.values
				fvalue = populated_fields[ffield]

				# additionally added for new rule implementation
				# as the value is now always treated as an iterable
				if len(rvalue) == 1: rvalue = rvalue[0]

				# check if value is a list
				rv_list = is_list(rvalue)
				fv_list = is_list(fvalue)

				# SIMPLE CASE: both are just single values
				if (rv_list or fv_list) is False:
					pair = EntityPair(rvalue, fvalue)
					entities[rfield] = [pair]
					allpairs.append(pair)
					continue

				# SPECIAL CASE: at least one value is a set of items
				if not is_list(rvalue): rvalue = [rvalue]
				if not is_list(fvalue): fvalue = [fvalue]
				# create any combination of items as possible entity pairs
				# TODO: A better solution, e.g. "semantic set similarity"?
				pairs = [EntityPair(v1, v2) for v1 in rvalue for v2 in fvalue]
				entities[rfield] = pairs  # assign pairs to field
				allpairs.extend(pairs)  # add all pairs

		return entities, allpairs

	# for performance stats
	def get_stats(self):
		""" Get stats dict used for evaluation. """
		if not self.gather_stats: return {}
		return self.stats

	def reset_stats(self):
		""" Reset stats dict. Called by pipeline. """
		if not self.gather_stats: return
		self.stats = {
			'multi_field_agg': 0
		}
