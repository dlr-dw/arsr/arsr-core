import json


class ScoredRule:
	""" Specific kind of rule created during scoring process. """

	def __init__(self, support, confidence):
		self._support = support
		self._confidence = confidence
		self.fieldscores = dict() # relatedness scores of fields
		self.scores = dict() # dict for additional scores like jaccard
		self.suggestions = set() # relevant consequent items as suggestions
		self.final = dict() # final scores, key = agg method, value = score
		self.final_score = 0 # final score for a single config method


	@property
	def support(self):
		return self._support

	@property
	def confidence(self):
		return self._confidence


	def add_score(self, name, value):
		""" Adds the score with given name and value to this rule. """
		self.scores[name] = value

	def add_scores(self, scores_dict):
		""" Adds entries of the scores dict to the local dict. """
		self.scores.update(scores_dict)

	def get_score(self, name, default=None):
		""" Returns score for given name or default (None) if not found. """
		if self.scores is None: return default
		return self.scores.get(name, default)


	def set_field_score(self, fieldname, score):
		""" Assign relatedness score for this fieldname. """
		self.fieldscores[fieldname] = score

	def get_field_score(self, fieldname, default):
		""" Get score of a field or default if the field does not exist. """
		return self.fieldscores.get(fieldname, default)

	def get_field_scores(self, scores_only=False):
		""" Returns a generator of (field, score) tuples or just scores. """
		if scores_only: return (score for score in self.fieldscores.values())
		return ((field, score) for field, score in self.fieldscores.items())


	def add_suggestion(self, suggestion):
		""" Add a consequent item of rule, eligable for recommendation. """
		self.suggestions.add(suggestion)

	def add_suggestions(self, suggestions):
		""" Add all items of the passed iterable to the suggestions. """
		self.suggestions.update(suggestions)

	def get_suggestions(self):
		""" Returns a generator to retrieve stored suggestions. """
		return (s for s in self.suggestions)


	def add_final_score(self, aggmethod, score):
		""" Adds the final score for given aggregation method. """
		self.final[aggmethod] = score

	def get_final_scores(self):
		""" Returns a generator to retrieve (aggmethod, score) tuples. """
		return ((m, s) for m, s in self.final.items())


	def set_final_score(self, score): self.final_score = score
	def get_final_score(self): return self.final_score


	def __str__(self):
		""" Print this rule information as JSON. """
		dinfo = {
			'support': self.support,
			'confidence': self.confidence,
		}
		if len(self.fieldscores) > 0: dinfo['fieldscores'] = self.fieldscores
		if len(self.scores) > 0: dinfo['scores'] = self.scores
		if len(self.suggestions) > 0: dinfo['suggestions'] = list(self.suggestions)
		if len(self.final) > 0: dinfo['final'] = self.final # eval
		else: dinfo['final'] = self.final_score # normal
		return json.dumps(dinfo)


	@classmethod
	def from_rule(cls, rule):
		""" Creates a scored rule from rulemining.rules.Rule instance. """
		return cls(rule.support(), rule.confidence())
