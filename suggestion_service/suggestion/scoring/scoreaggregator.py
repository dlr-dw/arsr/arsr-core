from .aggregation import AggregationMethods


class ScoreAggregator:
	""" Aggregates scores of the same suggestion value. """

	def __init__(self, aggmethod=None, evaluation=False, precision=None):
		""" Initialize in normal mode or evaluation mode.

		Parameters
		----------
		aggmethod : func(iterable) that returns a number
			Instance of an aggregation method - required for using normal mode.
		evaluation : bool
			Turn on/off evaluation mode where all aggregation methods are used.
		precision : int or None
			Number of digits for rounding final scores (or disabled if None).

		Raises
		------
		ValueError
			If there is no such aggregation method for given name.
		"""
		# TODO: maybe allow to pass a dict of evaluation methods with "name:func"
		self.method = aggmethod
		self.evalmode = evaluation

		# configuration
		self.precision = precision
		if precision is not None and precision < 0: self.precision = 0

	def aggregate(self, suggestions_dict):
		""" Aggregate scores of the same suggestion s.

		Returns
		-------
		dict
			In normal mode, a dict: {s: score}.
			In evaluation mode, a dict: {s: { method_f: { method_v: score } }}
			(method_f = field agg. method, method_v = value agg. method)
			Note that method_v was set on class initialization.
		"""
		if self.evalmode: return self.__agg_eval(suggestions_dict)
		return self.__agg_single(suggestions_dict) # normal mode

	def __agg_single(self, suggestions):
		""" Aggregate easy case with just a single aggregation method. """

		if self.method is None: raise ValueError('Missing aggregation method!')
		suggestions_scored = dict() # {s : score}
		for suggestion, sscores in suggestions.items():
			final_score = self.rnd(self.method(sscores))
			suggestions_scored[suggestion] = final_score
		return suggestions_scored

	def __agg_eval(self, suggestions):
		""" Aggregate complex case with every combination of agg. methods."""

		# suggestions_scored = dict()  # {s: { method_v: { method_f: score } }} # V1
		suggestions_scored = dict()  # {s: { method_f: { method_v: score } }} # V2

		for suggestion, sscores in suggestions.items():

			# gather all scores for each field aggregation method (method_f)
			fieldaggs = {}
			for method_f, score in sscores:
				if method_f not in fieldaggs: fieldaggs[method_f] = []
				fieldaggs[method_f].append(score)

			# compute for each final aggregation method (method_v)

			## V1 (store first the inner agg method, then the outer one)
			# info = {}
			# for method_f, fscores in fieldaggs.items():
			# 	for method_v in AggregationMethods.get_methods():
			# 		agg = AggregationMethods.get_method(method_v)
			# 		if not method_v in info: info[method_v] = {}
			# 		final_score = self.rnd(agg(fscores))
			# 		info[method_v][method_f] = final_score

			## V2 (store first the outer agg method, then the inner one)
			# This way, it is easier to read the JSON results.
			info = {}
			for method_f, fscores in fieldaggs.items():
				info[method_f] = {}
				for method_v in AggregationMethods.get_methods():
					agg = AggregationMethods.get_method(method_v)
					final_score = self.rnd(agg(fscores))
					info[method_f][method_v] = final_score

			suggestions_scored[suggestion] = info

		return suggestions_scored

	def rnd(self, value):
		""" Round score at specific precision or don't round at all. """
		if self.precision is None: return value
		return round(value, self.precision)
