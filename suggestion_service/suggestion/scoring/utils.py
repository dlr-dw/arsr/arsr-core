class Utils:
	""" Utils class with various helper methods. """

	@staticmethod
	def jaccard_index(set1, set2, equal_func=None):
		""" Computes the jaccard index of two sets.

		Parameters
		----------
		set1 : typing.Collection
			E.g. a list, set, tuple
		set2 : typing.Collection
		equal_func : func
			Optional function to compare items of both sets.
			If not set or None passed, use default equal operator.

		Returns
		-------
		float
		"""
		if equal_func is None: equal_func = lambda x, y: x == y
		inter = sum(equal_func(item1, item2) for item1 in set1 for item2 in set2)
		union = len(set1) + len(set2) - inter
		return inter / union
