import functools
import random
import statistics
import typing


class Methods:
	""" Actual method implementations. """

	@staticmethod
	def median(values):
		""" Median (0.5-quantile) of all values. """
		return statistics.median(values)

	@staticmethod
	def mean(values):
		""" Mean (average) of all values. """
		return statistics.mean(values)

	@staticmethod
	def minimum(values):
		""" Get the minimal value out of all. """
		return min(values)

	@staticmethod
	def maximum(values):
		""" Get the maximum value out of all. """
		return max(values)

	@staticmethod
	def product(values):
		""" Product of all values. """
		return functools.reduce(lambda x, y: x * y, values)

	@staticmethod
	def random(values):
		""" Get a random value out of all. """
		return random.choice(values)


class AggregationMethods(Methods):
	""" Interface to various score aggregation methods.
	Deriving from Methods makes them directly accessible through this class.
	"""

	# add new methods in class Methods and register them here
	__methods = {
		'median': Methods.median,
		'mean': Methods.mean,
		'min': Methods.minimum,
		'max': Methods.maximum,
		'product': Methods.product,
		'random': Methods.random
	}

	@classmethod
	def get_methods(cls):
		""" Get names of available methods. """
		return cls.__methods.keys()

	@classmethod
	def get_method(cls, name: str, default=None) -> typing.Union[typing.Callable, None]:
		""" Get a method by its name or default if not found. """
		return cls.__methods.get(name, default)
