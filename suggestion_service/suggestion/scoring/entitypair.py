class EntityPair:
	""" Class representing a pair of entities and their relatedness.
	Instances are hashed only by their first and second property.
	"""

	def __init__(self, first, second):
		self._first = str(first)
		self._second = str(second)
		self._relatedness = 0
		self._relatedness_set = False

	@property
	def first(self):
		return self._first

	@property
	def second(self):
		return self._second

	@property
	def relatedness(self):
		return self._relatedness

	@relatedness.setter
	def relatedness(self, value):
		self._relatedness = value
		self._relatedness_set = True

	@property
	def relatedness_set(self):
		return self._relatedness_set


	def __str__(self):
		return f'({self.first}, {self.second}) - rel: {self.relatedness}'

	def __repr__(self):
		return f'({self.first}, {self.second}, {self.relatedness})'

	# def __eq__(self, other):
	# 	""" Checks if two entity pairs are the same. """
	# 	# Objects of this class are no longer hashable because we
	# 	# implemented this method -> need to implement __hash__ as well.
	# 	return (self.first == other.first and self.second == other.second
	# 		and self.relatedness == other.relatedness)

	# def __hash__(self):
	# 	""" Allows hashing instances based on first and second. """
	# 	# https://docs.python.org/3/reference/datamodel.html#object.__hash__
	# 	# Note that relatedness does not play a role here.
	# 	# This way, we add only one instance to sets and dicts.
	# 	return hash((self._first, self._second))
