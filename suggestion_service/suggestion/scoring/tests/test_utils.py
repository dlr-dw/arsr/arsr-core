import unittest

from ..utils import Utils


class TestUtils(unittest.TestCase):
	""" Contains tests for scoring utils. """

	def test_jaccard_index(self):
		""" Test computation of jaccard index. """
		set1 = {1, 2, 3, 4, 5}
		set2 = {1, 2, 3, 4, 5}
		ji = Utils.jaccard_index(set1, set2)
		self.assertEqual(ji, 1)

		set1 = {1, 2, 3, 8, 9}
		set2 = {1, 2, 3, 4}
		ji = Utils.jaccard_index(set1, set2)
		self.assertEqual(ji, 3/6)

		# check string support and set {}
		set1 = {'A', 'B', 'C', 'X', 'Z'}
		set2 = {'A', 'B', 'C', 'D'}
		ji = Utils.jaccard_index(set1, set2)
		self.assertEqual(ji, 3/6)

		# check order independence and list []
		set1 = ['X', 'Z', 'A', 'B', 'C']
		set2 = ['A', 'B', 'C', 'D']
		ji = Utils.jaccard_index(set1, set2)
		self.assertEqual(ji, 3/6)

		set1 = {1, 2, 3}
		set2 = {4, 5, 6}
		ji = Utils.jaccard_index(set1, set2)
		self.assertEqual(ji, 0)
