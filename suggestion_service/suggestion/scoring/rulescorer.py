from .aggregation import AggregationMethods
import typing


class RuleScorer:
	""" Component to score the entire rule.
	Aggregates the field values to a final rule score.
	"""

	def __init__(self, aggmethod: typing.Callable[[typing.Iterable], float] = None, evaluation=False):
		""" Initialize in normal mode or evaluation mode.

		Parameters
		----------
		aggmethod : typing.Callable[[typing.Iterable], float]
			Instance of an aggregation method - required for using normal mode.
		evaluation : bool
			Turn on/off evaluation mode where all aggregation methods are used.

		Raises
		------
		ValueError
			If there is no such aggregation method for given name.
		"""
		# TODO: maybe allow to pass a dict of evaluation methods with "name:func"
		self.method = aggmethod
		self.evalmode = evaluation

		# default configuration
		self._skip_zero_score = True
		self._apply_confidence = True
		self._apply_jaccard_index = True
		self._apply_consequent_fit = False

	def skip_zero_score(self, skip):
		"""	Skip rules with a final score of zero. """
		if skip is None: return
		self._skip_zero_score = skip

	def apply_confidence(self, apply):
		""" Apply (multiply) rule confidence to final score. """
		if apply is None: return
		self._apply_confidence = apply

	def apply_jaccard_index(self, apply):
		""" Apply (multiply) jaccard index of rule antecedent to final score. """
		if apply is None: return
		self._apply_jaccard_index = apply

	def apply_consequent_fit(self, apply):
		""" Apply (multiply) rule consequent fit to final score. """
		if apply is None: return
		self._apply_consequent_fit = apply

	def get_config_str(self):
		""" Get current configuration as a string. """
		return f'skip_zero_score: {self._skip_zero_score}, ' \
			+ f'apply_confidence: {self._apply_confidence}, ' \
			+ f'apply_jaccard_index: {self._apply_jaccard_index}, ' \
			+ f'apply_consequent_fit: {self._apply_consequent_fit}'

	def score(self, scored_rule, fieldscores):
		""" Set final score(s) of the rule based on the fieldscores.

		A set of field scores is aggregated using an aggregation method.
		If multiple methods are used depends on the "evaluation mode".

		Parameters
		----------
		scored_rule : ScoredRule
			The scored rule instance used in the pipeline.
		fieldscores : iterable(list, set, tuple)
			A list of field scores to aggregate.

		Returns
		-------
		bool
			True, if the rule should be discarded
			(e.g. because it has a final score of zero).
		"""
		if len(fieldscores) == 0:
			fieldscores = [0]  # avoids failure of aggregation methods

		if self.evalmode:
			has_zero_score = self.__score_eval(scored_rule, fieldscores)
		else:
			has_zero_score = self.__score_single(scored_rule, fieldscores)

		return self._skip_zero_score and has_zero_score

	def __score_single(self, scored_rule, fieldscores):
		""" Set final score of the rule based on the fieldscores. """
		if self.method is None: raise ValueError('Missing aggregation method!')
		ctx_score = self.method(fieldscores)  # context matching score
		rec_score = self.__combine(ctx_score, scored_rule)  # recommendation score
		scored_rule.set_final_score(rec_score)
		return rec_score <= 0

	def __score_eval(self, scored_rule, fieldscores):
		""" Method for evaluation using all aggregation methods. """
		all_zero = True
		for name in AggregationMethods.get_methods():
			method = AggregationMethods.get_method(name)
			if method is None: continue
			ctx_score = method(fieldscores)  # context matching score
			rec_score = self.__combine(ctx_score, scored_rule)
			scored_rule.add_final_score(name, rec_score)
			if rec_score > 0: all_zero = False
		return all_zero

	def __combine(self, ctx_score, scored_rule):
		""" Combine ctx_score with additional rule scores like confidence to get the final score.

		Parameters
		----------
		ctx_score : float
			The context_matching_score of the rule suggestions.
		scored_rule : ScoredRule
			The rule information instance holding required scores.

		Returns
		-------
		float
			The "recommendation_score(v)" of the suggestion value v.
		"""
		rec_score = ctx_score

		if self._apply_confidence:
			rec_score *= scored_rule.confidence

		if self._apply_jaccard_index:
			rec_score *= scored_rule.get_score('jaccard_index', 1)

		if self._apply_consequent_fit:
			rec_score *= scored_rule.get_score('consequent_fit', 1)

		return rec_score
