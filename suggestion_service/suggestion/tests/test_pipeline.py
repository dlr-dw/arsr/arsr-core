import unittest

from suggestion_service.rulemining.rules import RuleItem, Rule
from suggestion_service.rulemining.rules import repositories as rulerepos

from ..pipeline import PipelineConfig, Pipeline
from ..rproviders import BinaryProvider


class TestPipeline(unittest.TestCase):
	""" Contains tests for the scoring pipeline. """

	def setUp(self):
		pass

	def test_basic(self):
		""" Test basic workflow. """

		def item(field, value):
			return RuleItem(field, value)

		# setup pipeline
		rprovider = BinaryProvider()
		pconfig = PipelineConfig({
			'field_aggregation_method': 'mean',
			'score_aggregation_method': 'mean'
		})
		pipeline = Pipeline(rprovider, pconfig)

		# prepare data
		rulerepo = rulerepos.InMemoryRuleRepo()
		rulerepo.add_rules([
			Rule(
				[item('A', 'a'), item('B', 'b')], [item('C', 'c')],
				0.5, 0.8
			),
			Rule(
				[item('A', 'a')], [item('C', 'c'), item('N', 'n')],
				0.1, 0.2
			)
		])

		populated_fields = {'A': 'a', 'B': 'b'}
		target_field = 'C'

		# run pipeline
		s = pipeline.score(rulerepo.get_rules(), populated_fields, target_field)

		# computation
		# - Rule 1:
		#   - antecedent:
		#     - a == a for A => field score 1.0
		#     - b == b for B => field score 1.0
		#   - rec_score = mean(1.0, 1.0) = 1.0
		#   - jac_ind (of antecedent) = 2/2 = 1.0
		#   - rule score = rec_score * rule_confidence * jac_ind
		#     = 1 * 0.8 * 1.0 = 0.8
		# - Rule 2:
		#   - antecedent:
		#     - a == a for A => field score 1.0
		#   - rec_score = mean(1.0) = 1.0
		#   - jac_ind (of antecedent) = 1/2 = 0.5
		#   - rule score = 1.0 * 0.2 * 0.5 = 0.1
		# - Finally (for c of Field C)
		#   - mean(0.8, 0.1) = 0.45
		self.assertEqual(s, {'c': 0.45})
