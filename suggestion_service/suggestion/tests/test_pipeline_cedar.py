import unittest

from suggestion_service.rulemining.rules import RuleItem, Rule
from suggestion_service.rulemining.rules import repositories as rulerepos

from ..pipeline import CedarPipeline


class TestCEDARPipeline(unittest.TestCase):
	""" Contains tests for the scoring pipeline. """

	def setUp(self):
		# Rules based on the 2019 Paper, Table 2
		self.example_rules = [
			Rule(
				[RuleItem('disease', 'meningitis')],
				[RuleItem('tissue', 'brain')],
				3/6, 1
			),
			Rule(
				[RuleItem('tissue', 'brain')],
				[RuleItem('disease', 'meningitis')],
				3/6, 1
			),
			Rule(
				[RuleItem('disease', 'liver cancer')],
				[RuleItem('tissue', 'liver')],
				2/6, 1
			),
			Rule(
				[RuleItem('sex', 'male'), RuleItem('disease', 'meningitis')],
				[RuleItem('tissue', 'brain')],
				2/6, 1
			),
			Rule(
				[RuleItem('sex', 'male'), RuleItem('tissue', 'brain')],
				[RuleItem('disease', 'meningitis')],
				2/6, 1
			),
			Rule(
				[RuleItem('sex', 'female')],
				[RuleItem('tissue', 'brain')],
				1/6, 1
			),
			Rule(
				[RuleItem('sex', 'female')],
				[RuleItem('disease', 'meningitis')],
				1/6, 1
			),
			Rule(
				[RuleItem('disease', 'cirrhosis')],
				[RuleItem('tissue', 'liver')],
				1/6, 1
			),
			Rule(
				[RuleItem('sex', 'male'), RuleItem('disease', 'liver cancer')],
				[RuleItem('tissue', 'liver')],
				1/6, 1
			),
			Rule(
				[RuleItem('sex', 'male'), RuleItem('tissue', 'liver')],
				[RuleItem('disease', 'liver cancer')],
				1/6, 1
			),
			Rule(
				[RuleItem('sex', 'female'), RuleItem('disease', 'meningitis')],
				[RuleItem('tissue', 'brain')],
				1/6, 1
			),
			Rule(
				[RuleItem('sex', 'female'), RuleItem('tissue', 'brain')],
				[RuleItem('disease', 'meningitis')],
				1/6, 1
			),
			Rule(
				[RuleItem('tissue', 'brain')],
				[RuleItem('sex', 'male')],
				2/6, 2/3
			),
			Rule(
				[RuleItem('sex', 'male')],
				[RuleItem('tissue', 'brain')],
				2/6, 2/3
			),
			Rule(
				[RuleItem('disease', 'meningitis')],
				[RuleItem('sex', 'male')],
				2/6, 2/3
			),
			Rule(
				[RuleItem('sex', 'male')],
				[RuleItem('disease', 'meningitis')],
				2/6, 2/3
			),
			Rule(
				[RuleItem('tissue', 'liver')],
				[RuleItem('disease', 'liver cancer')],
				2/6, 2/3
			),
			Rule(
				[RuleItem('tissue', 'brain')],
				[RuleItem('disease', 'meningitis')],
				2/6, 2/3
			)
		]

	def test_basic(self):
		""" Test basic workflow. """
		rulerepo = rulerepos.InMemoryRuleRepo()
		rulerepo.add_rules(self.example_rules)

		target_field = 'tissue'
		populated_fields = {
			'disease': 'meningitis'
		}

		# prepare filter
		filterinfo = rulerepos.RuleFilterInfo()
		filterinfo.target_field = target_field
		# set it to only a single item with a single value
		filterinfo.consequent_items = rulerepos.RuleFilterInfo.ConsequentItemsEnum.SINGLE

		pipeline = CedarPipeline(discard_zero_score=False)
		suggestions = pipeline.score(
			rulerepo.get_rules_filtered(filterinfo),
			populated_fields,
			target_field
		)

		self.assertIn('brain', suggestions)
		self.assertEqual(suggestions['brain'], 1.0)
		self.assertIn('liver', suggestions)
		self.assertEqual(suggestions['liver'], 0.0)
