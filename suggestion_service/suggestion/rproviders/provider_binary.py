from .provider import RProvider


class BinaryProvider(RProvider):
	""" Simple provider for binary relatedness (1 = match, 0 = no match). """

	def __init__(self, **kwargs):
		super().__init__()

	def relatedness(self, entitypairs):
		for pair in entitypairs:
			score = 0
			if pair.first.lower() == pair.second.lower(): score = 1
			pair.relatedness = score
		return True
