# std
import logging

# third-party
import time

import requests

# local
from .provider import RProvider


class LDSProvider(RProvider):
	""" Default semantic relatedness provider.

	Requests semantic relatedness of entities from a REST-endpoint using the
	LDS-library (Java) as its backbone. It provides various methods like
	Linked Data Semantic Distance (LDSD) or Resource Similarity (Resim).
	"""

	def __init__(self, endpoint_url, cache=None, request_timeout=None,
		require_uris=True, gather_stats=False, use_session=True):
		"""
		Parameters
		----------
		endpoint_url : str
			URL to the endpoint that receives the requests.
		cache : Mapping
			An object, container or class that acts like a dict.
			It must provide get(key, default) and set per [key] = value.
			We recommend using a library like cachetools.
		request_timeout : float
			Timeout for requests to the endpoint in seconds. Default: None.
			Using None disables this feature and waits indefinitely.
		require_uris : bool
			Require that entity values are URIs and if not, a simple string-
			similarity is used to rate their relatedness. Default: True.
			Used string similarity can be changed with set_string_sim(callable).
		gather_stats : bool
			Mode to store additional stats for evaluation.
		use_session : bool
			Use http session to keep connection alive for multiple requests.
		"""
		super().__init__()
		self.logger = logging.getLogger(__name__)
		self._endpoint_url = endpoint_url
		self.cache = cache
		self.timeout = request_timeout
		self.require_uris = require_uris
		self.string_sim = lambda x, y: int(x.lower() == y.lower())

		# use session to keep connection alive for multiple requests
		self.http_session = None
		if use_session: self.http_session = requests.Session()

		# store stats for evaluation
		self.gather_stats = gather_stats
		self.stats = {}
		self.reset_stats()

	def __get_cached(self, e1, e2):
		""" Get value if this entity pair is already cached or None. """
		if self.cache is None: return None
		cval = self.cache.get((e1, e2), None)
		return cval

	def __add_cache(self, e1, e2, value):
		""" Add the value for this entity pair to the cache. """
		if self.cache is None: return
		self.cache[(e1, e2)] = value

	def relatedness(self, entitypairs):
		# variables for performance stats
		cnt_no_uri = 0
		cnt_entity_pairs = 0

		# check if already cached and use this value instead
		# also checks that values are URIs if required
		entities = []
		for pair in entitypairs:

			cnt_entity_pairs += 1
			cachedval = self.__get_cached(pair.first, pair.second)
			if cachedval is not None:
				pair.relatedness = cachedval
				continue

			# Fallback solution to use string-sim. if values are no URI.
			# we don't cache the result as this is more important for requests
			both_uris = self._is_uri(pair.first) and self._is_uri(pair.second)
			if self.require_uris and not both_uris:
				pair.relatedness = self.string_sim(pair.first, pair.second)
				cnt_no_uri += 1
				continue

			entities.append({'a': pair.first, 'b': pair.second})

		# store performance stats
		if self.gather_stats:
			self.stats['entity_pairs_total'] += cnt_entity_pairs
			self.stats['entity_pairs_nouri'] += cnt_no_uri
			self.stats['lds_requests'] += 1 if len(entities) > 0 else 0

		# done if there are no requests to do
		if len(entities) == 0: return True

		# send request to endpoint (can raise errors)
		t0 = time.perf_counter()
		try:
			request_data = {'entities': entities}
			pairs_rated = self.__request_lds(request_data)
		except Exception as ex:
			self.logger.error(f'Failed to retrieve relatedness from endpoint!')
			# TODO: handle here and return False instead of raising?
			raise ex
		t_request = round(time.perf_counter() - t0, 4)
		if self.gather_stats:
			self.stats['lds_requests_time'] += t_request

		# add relatedness to pairs that have not set it yet
		t0 = time.perf_counter()
		for pair in entitypairs:
			if pair.relatedness_set: continue

			# check if already cached
			cachedval = self.__get_cached(pair.first, pair.second)
			if cachedval is not None:
				pair.relatedness = cachedval
				continue

			# find corresponding entry in response JSON and add to cache
			# TODO: we could improve it by adding ids to entities
			#   but this would require changing the relatedness API
			for rated in pairs_rated:
				first = rated.get('a')
				second = rated.get('b')
				relatedness = rated.get('r')
				if pair.first == first and pair.second == second and relatedness is not None:
					pair.relatedness = relatedness
					self.__add_cache(pair.first, pair.second, relatedness)
					break

		t_assign_ms = round((time.perf_counter() - t0) * 1000, 4)
		self.logger.debug(f'Time for request={t_request}s, assignment={t_assign_ms}ms')  # DEBUG
		return True

	def __request_lds(self, request_data):
		""" Request relatedness of entity pairs from LDSProvider endpoint.

		LDSProvider requires the structure:
		{
			"entities": [
				{"a": "firstEntity", "b": "secondEntity"},
				...
			]
		}
		... and returns the following:
		{
			"entities": [
				{"a": "firstEntity", "b": "secondEntity", "r": 0.123},
				...
			]
		}

		Parameters
		----------
		request_data : dict
			The structure of entity pairs as described above.

		Returns
		-------
		list | None
			List of entity pair dicts with attribute "r" as described above.

		Raises
		------
		requests.exceptions.HTTPError
			If connection to the endpoint failed and for bad status codes.
		requests.exceptions.JSONDecodeError
			If the response could not be decoded to valid JSON.
		"""
		# Uses "requests": https://docs.python-requests.org/en/latest
		if self.http_session is None:
			r = requests.post(self._endpoint_url, json=request_data, timeout=self.timeout)
		else:
			r = self.http_session.post(self._endpoint_url, json=request_data, timeout=self.timeout)
		r.raise_for_status()  # raise exception for bad status codes (4XX 5XX)
		result = r.json()  # raises exception on failure
		return result.get('entities', None)

	@staticmethod
	def _is_uri(value):
		""" Checks if the passed value is a URI and returns True if so.
		This is just a simple check for http and https prefix.
		The method is currently not validating a URI.
		"""
		value = str(value)
		if not (value.startswith('http://') or value.startswith('https://')):
			return False
		return True

	def set_string_sim(self, string_sim):
		""" Set string similarity method used if one pair value is no URI.

		Parameters
		----------
		string_sim : callable
			A callable that takes two parameters, one for each entity
			and returns a similarity score between 0 and 1, where 0
			represents no similarity and 1 represents equality.
		"""
		if not callable(string_sim):
			raise ValueError('Passed string similarity is not callable')
		self.string_sim = string_sim

	# ----------------------------------
	# methods for performance stats

	def get_stats(self):
		""" Returns a dict of stats for evaluation. """
		if not self.gather_stats: return {}
		return self.stats

	def reset_stats(self):
		if not self.gather_stats: return
		self.stats = {
			'entity_pairs_nouri': 0,
			'entity_pairs_total': 0,
			'lds_requests': 0,
			'lds_requests_time': 0
		}
