# third-party lib for caching
import cachetools

from . import BinaryProvider, LDSProvider


class RProviderFactory:
	""" Factory to instantiate available relatedness providers. """

	@classmethod
	def create(cls, provider, **kwargs):
		""" Create an instance of a relatedness provider with given settings.

		Parameters
		----------
		provider : str
			Name of the provider to create an instance of (case insensitive).
		**kwargs
			Arguments to pass as settings to the constructors of providers.

		Returns
		-------
		RProvider
			An instance of the according relatedness provider.

		Raises
		------
		ValueError
			If there is no such provider for the given name.
		"""

		provider = provider.lower().strip()
		if provider == 'binary': return BinaryProvider(**kwargs)

		if provider == 'lds':
			c_cache = kwargs.get('cache', None)
			if c_cache is not None: kwargs['cache'] = cls.prepare_cache(c_cache)
			return LDSProvider(**kwargs)

		raise ValueError('Invalid relatedness provider!')

	@classmethod
	def prepare_cache(cls, conf):
		""" Instantiate default TTL Cache and return it.
		Type of the cache can be set by assigning a value to field "type".
		Default cache type is LRU, supported are currently: "lru", "lfu", "ttl".
		Each cache requires at least "maxsize" to be set or default of 200 will be used.
		TTL also requires "ttl" to be set (time-to-live = how long items remain in seconds).
		"""
		cache_type = str(conf.get('type', 'lru')).upper()
		maxsize = int(conf.get('maxsize', 200))
		if cache_type == 'LRU':
			return cachetools.LRUCache(maxsize)
		if cache_type == 'LFU':
			return cachetools.LFUCache(maxsize)
		if cache_type == 'TTL':
			# ttl = time-to-live (how long items remain in cache in sec.)
			return cachetools.TTLCache(maxsize, ttl=conf.get('ttl', 300))
		raise ValueError(f'invalid cache type: {cache_type}')
