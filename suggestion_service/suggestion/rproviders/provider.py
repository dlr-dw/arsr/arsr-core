from ..scoring.entitypair import EntityPair


class RProvider:
	""" Relatedness Provider base class. """

	def __init__(self):
		pass

	def relatedness(self, entitypairs):
		""" Get relatedness of each entity pair.

		Parameters
		----------
		entitypairs : typing.Iterable[EntityPair]
			A set of EntityPair objects.

		Returns
		-------
		bool
			True on success and False otherwise.
		"""
		return False

	def close(self):
		""" Close connections and free resources. """
		pass

	# for performance stats
	def get_stats(self):
		""" Used for evaluation to retrieve some stats. """
		return {}

	def reset_stats(self):
		""" Used for evaluation to reset stats. """
		pass
