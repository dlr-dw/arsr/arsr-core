import logging
import suggestion_service.rulemining.rules.repositories as rulerepos
from . import rproviders
from . import Pipeline, PipelineConfig


class SuggestionService:
	""" Main component of suggestion module to start generating suggestions. """

	def __init__(self, scoring_pipeline):
		self.logger = logging.getLogger(__name__)
		self.pipeline = scoring_pipeline

	def generate_suggestions(self, rulerepo, populated_fields, target_field, rulefilterinfo=None):
		""" Generate suggestions for target field based on populated fields.

		Parameters
		----------
		rulerepo : RuleRepo
			Rule repository to use for retrieving association rules.
		populated_fields : dict
			A dictionary of currently populated fields, e.g.:
			{
				"f1_label": "f1_value",
				"f2_label": ["f2_v1", "f2_v2", ...],
				...
			}
		target_field : str
			Name (label) of target field for which suggestions are generated.
		rulefilterinfo : RuleFilterInfo
			Provides settings and data for rule filtering. If None is passed,
			default settings are used (e.g. target field in consequent).

		Returns
		-------
		dict
			Dictionary of suggestions. In case of evaluation mode, each key is
			a suggestion and contains combinations of used aggregation methods
			and score. Otherwise, key is a suggestion and value its score.
		"""

		if not isinstance(rulerepo, rulerepos.RuleRepo):
			raise ValueError('Invalid rule repository!')

		# use default rule filter settings and add required data
		if rulefilterinfo is None: rulefilterinfo = rulerepos.RuleFilterInfo()
		rulefilterinfo.target_field = target_field

		if rulerepo.is_empty():
			self.logger.info('No rules in form repository.')
			return None

		# TODO: consider validating format of populated fields input as it can come from API
		#  (e.g. could use new FormInstance class for populated fields)

		# start scoring pipeline
		scored_suggestions = self.pipeline.score(
			rulerepo.get_rules_filtered(rulefilterinfo),
			populated_fields,
			target_field
		)

		return scored_suggestions

	@classmethod
	def example_setup(cls, evaluation=False):
		# create scoring pipeline and service instance
		# the config will have all default settings plus those defined here
		config = PipelineConfig({
			'evaluation': evaluation,
			'field_aggregation_method': 'median',
			'score_aggregation_method': 'median',
			'skip_zero_score': True
		})
		print(f'Config: {config}')
		relatedness_provider = rproviders.BinaryProvider()
		pipeline = Pipeline(relatedness_provider, config, verbosity=1)
		return cls(pipeline)
