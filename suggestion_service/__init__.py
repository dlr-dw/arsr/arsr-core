#!/usr/bin/env python3
"""
Suggestion service implementation.
Leon H. - 2022
"""

__version__ = "1.0.0"

import sys
import logging
from . import rulemining, suggestion, io

if (sys.version_info[0] < 3) or (sys.version_info[1] < 6):
	msg = "The suggestion_service package only works for Python 3.6+."
	raise Exception(msg)

# Add NullHandler to main logger for this package.
# Logging in app can be disabled using:
# logging.getLogger('suggestion_service').propagate = False
logging.getLogger(__name__).addHandler(logging.NullHandler())
