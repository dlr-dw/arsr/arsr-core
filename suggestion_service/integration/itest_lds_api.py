# Run this integration test using one of the following commands:
# python -m suggestion_service.integration.itest_lds_api
# python -m unittest discover -p itest_*.py
# This test is not started with python -m unittest as it requires that the LDS
# service is running and that specific data is already loaded to the RDF store.

import os.path
import unittest
import time

# third-party lib for caching
import cachetools

from .. import rulemining
from ..rulemining.forms import repositories as formrepos
from ..rulemining.rules import repositories as rulerepos
from ..rulemining.methods import MethodFactory

from .. import suggestion
from ..suggestion import rproviders
from ..suggestion.scoring import EntityPair


class TestLDSDAPI(unittest.TestCase):
	""" Testing the LDS-API.
	
	Note that the RDF store should contain the following data:
	data/example/rdf/animal-example-data.ttl

	The tests use the lds provider (Java API for using LDS impl.),
	check below that the endpoints are correct and available.
	"""

	def setUp(self):
		""" Prepare for tests. """
		self.FIXTURE_DIR = 'fixtures'
		self.LDS_ENDPOINT_LDSD = 'http://localhost:8080/api/relatedness/ldsd'
		self.LDS_ENDPOINT_RESIM = 'http://localhost:8080/api/relatedness/resim'

		thisdirpath = os.path.dirname(__file__)
		self.filesdir = os.path.join(thisdirpath, self.FIXTURE_DIR)
		# print(f'Using files directory: {filesdir}')

	def test_basic_connection(self):
		""" Tests if basic requests can be send and responses received. """
		ldsprov = rproviders.LDSProvider(self.LDS_ENDPOINT_LDSD)
		dbr = 'http://dbpedia.org/resource/'
		pairs = (
			EntityPair(f'{dbr}Cat', f'{dbr}Dog'),
			EntityPair(f'{dbr}Horse', f'{dbr}Mouse'),
			EntityPair(f'{dbr}Bear', f'{dbr}Fish'),
		)

		# add relatedness score to entity pairs
		ldsprov.relatedness(pairs)
		for pair in pairs:
			self.assertTrue(
				pair.relatedness_set,
				f'Relatedness not set for pair: ({pair.first}, {pair.second})'
			)

	def test_same_sim(self):
		""" Tests if rel(a,a)=1 holds true. """
		ldsprov = rproviders.LDSProvider(self.LDS_ENDPOINT_RESIM)
		dbr = 'http://dbpedia.org/resource/'
		pairs = (
			EntityPair(f'{dbr}Bear', f'{dbr}Bear'),
			EntityPair(f'{dbr}Cat', f'{dbr}Cat'),
		)
		ldsprov.relatedness(pairs)
		for pair in pairs:
			self.assertEqual(
				pair.relatedness, 1,
				f'rel({pair.first}, {pair.second}) != 1'
			)

	def test_basics_animal_example(self):
		""" Tests basic functionality using animal example data. """
		datapath = os.path.join(self.filesdir, 'animal_instances_1.json')
		is_legacy_data_format = True  # if the file uses the old format
		self.assertTrue(os.path.exists(datapath))

		# load instances
		formrepo = formrepos.FileFormRepo(datapath, legacy_data=is_legacy_data_format)
		rulerepo = rulerepos.InMemoryRuleRepo()

		# mine rules using apriori
		apriori = MethodFactory.create(
			'apriori',
			minsupp=0.1,
			minconf=0.5
		)
		arms = rulemining.RuleMiningService()
		arms.generate_rules(formrepo, apriori, rulerepo)

		# set up relatedness provider with caching
		# ttl = time-to-live (how long items remain max. in cache in sec.)
		cache = cachetools.TTLCache(maxsize=200, ttl=300)
		ldsprovider = rproviders.LDSProvider(
			self.LDS_ENDPOINT_LDSD,
			cache=cache
		)

		# configure scoring pipeline
		config = suggestion.PipelineConfig({
			'evaluation': False,
			'field_aggregation_method': 'median',
			'score_aggregation_method': 'max',
			'skip_zero_score': True,
			'jaccard_threshold': 0.8
		})
		config.validate()

		print(f'Pipeline config: {config}')
		pipeline = suggestion.Pipeline(
			ldsprovider,
			config, verbosity=0
		)

		# define user input
		# avail. fields: name, type, eats, tags
		target_field = 'name'
		populated_fields = {
			'type': 'http://dbpedia.org/ontology/Mammal',
			'eats': [
				'http://dbpedia.org/resource/Carrot'
			]
		}

		print('Starting scoring pipeline...')
		t0 = time.perf_counter()
		sugs = suggestion.SuggestionService(pipeline)
		scored_suggestions = sugs.generate_suggestions(
			rulerepo, populated_fields, target_field)

		t_required = time.perf_counter() - t0
		print(f'Time required: {t_required:.2f} sec.')
		print(f'Suggestions: {scored_suggestions}')
		# TODO: maybe add some assert checks

	def test_no_uri(self):
		""" Use string similarity if any entity of a pair is no URI. """
		ldsprov = rproviders.LDSProvider(self.LDS_ENDPOINT_LDSD)
		ldsprov.set_string_sim(lambda x, y: int(x.lower() == y.lower()))
		dbr = 'http://dbpedia.org/resource/'
		pairs = (
			EntityPair(f'{dbr}Bear', f'Bear'),
			EntityPair('Cat', f'{dbr}Cat'),
			EntityPair('yes', 'yes'),
			EntityPair('yes', 'no'),
			EntityPair('no', 'yes')
		)
		ldsprov.relatedness(pairs)
		for pair in pairs:
			expected = 1 if pair.first.lower() == pair.second.lower() else 0
			self.assertEqual(pair.relatedness, expected)


if __name__ == '__main__':
	unittest.main()
