from pathlib import Path


class IOUtils:
	""" Helper methods for IO. """

	@staticmethod
	def prepare_file_write(filepath: str):
		""" Creates file and the directories if missing.
		Returns the pathlib.Path or None if file is invalid.
		"""
		path = Path(filepath)
		if not path.exists(): # try creating
			path.parent.mkdir(parents=True, exist_ok=True) # create dirs
			try: path.open('x').close() # create file
			except FileExistsError: pass
		if not path.is_file(): return None
		return path
