from setuptools import setup, find_packages

# Note: Don't import anything from suggestion_service yet, as dependencies could be missing.

setup(
	name='suggestion_service',
	version='1.0.0',
	description='Pipeline for a recommender system, based on association rule mining and semantic relatedness.',
	long_description=(
		'The concept of this recommender is based on the work of Martinez-Romero et al. from 2019.'
		'Association rule mining is used to extract relevant rules, which are then used by a scoring pipeline'
		'to create recommendations. This work changes the scoring procedure and extends it with semantic relatedness.'
		'Furthermore, fields with multiple values are supported in a first possible way.'
		'To use the semantic relatedness feature, a relatedness provider called LDS exists.'
		'An API is used to communicate with the Java implementation, which provides LDSD and Resim.'
	),
	keywords=['recommender system', 'association rule mining', 'semantic relatedness'],
	author='Leon Hutans',
	author_email='leon.hutans@uni-jena.de',
	#packages=['suggestion_service'],  # does not find sub-packages!
	packages=find_packages('.', include=['suggestion_service*']),
	install_requires=[
		'efficient-apriori == 2.0.1',
		'requests >= 2.27.1',
		'cachetools >= 5.0.0',
		'jsonschema >= 4.4.0',
		'regex >= 2021.11.10'
	]
)
